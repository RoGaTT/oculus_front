type WordEndingDictItem = {
  [key: string]: string[][]
}

const wordEndingDict: WordEndingDictItem = {
  лекци: [
    ['я'],
    ['и'],
    ['й'],
  ],
};

export default wordEndingDict;
