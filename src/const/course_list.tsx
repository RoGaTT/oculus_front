import { CourseData, CourseMonthEnum } from '@/types/course';

import AuthorImg1 from '@/assets/img/temp/author_photo_1.webp';
import AuthorImg2 from '@/assets/img/temp/author_photo_2.webp';
import AuthorImg3 from '@/assets/img/temp/author_photo_3.webp';
import AuthorImg4 from '@/assets/img/temp/author_photo_4.webp';
import LectureImg4 from '@/assets/img/temp/course_banner_4.webp';
import LectureImg5 from '@/assets/img/temp/course_banner_5.webp';

import AuthorFallbackImg1 from '@/assets/img/fallback/temp/author_photo_1.png';
import AuthorFallbackImg2 from '@/assets/img/fallback/temp/author_photo_2.png';
import AuthorFallbackImg3 from '@/assets/img/fallback/temp/author_photo_3.png';
import AuthorFallbackImg4 from '@/assets/img/fallback/temp/author_photo_4.png';
import LectureFallbackImg4 from '@/assets/img/fallback/temp/course_banner_4.png';
import LectureFallbackImg5 from '@/assets/img/fallback/temp/course_banner_5.png';

const formatIframe = (html: string): string => {
  let data = html.replace(/height=["']?[\d]*["']?/gi, '');
  data = data.replace(/width=["']?[\d]*["']?/gi, '');
  return data;
};

const courseList: CourseData[] = [
  {
    authors: [
      {
        name: 'Антонов Алексей Анатольевич',
        info: 'ведущий научный сотрудник, к.м.н., ФГБНУ «НИИГБ», Москва',
        photo: AuthorImg2,
        fallbackPhoto: AuthorFallbackImg2,
      },
      {
        name: 'Брежнев Андрей Юрьевич',
        info: 'доцент, к.м.н., ФГБОУ ВО КГМУ МЗ РФ, Курск',
        photo: AuthorImg4,
        fallbackPhoto: AuthorFallbackImg4,
      },
      {
        name: 'Карлова Елена Владимировна ',
        info: 'д.м.н. доцент кафедры офтальмологии Самарского государственного медицинского университета. Зам. главного врача по инновационно-технологическому развитию, врач-офтальмолог высшей категории',
        photo: AuthorImg3,
        fallbackPhoto: AuthorFallbackImg3,
      },
      {
        name: 'Куроедов Александр Владимирович',
        info: 'д.м.н., начальник офтальмологического отделения ФКУ «ЦВКГ им. Мандрыка» МО РФ, профессор кафедры офтальмологии им. акад. А.П. Нестерова лечебного факультета ФГАОУ ВО РНИМУ им. Пирогова МЗ РФ, член Президиума РГО',
        photo: AuthorImg1,
        fallbackPhoto: AuthorFallbackImg1,
      },
    ],
    description: 'Коллектив лекторов рассматривает случаи пациентов с диагнозом «ПОУГ», место комбинированных препаратов в терапии глаукомы, влияние приверженности лечению на динамику заболевания, ступенчатую терапию глаукомы и принципы ее применения, клинико-эпидемиологические исследования и их роль в изучении глаукомы.',
    isActive: true,
    startMonth: CourseMonthEnum.MAY,
    title: 'Глаукома. От теории к практике.',
    lectures: [
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        video: formatIframe('<iframe src="https://player.vimeo.com/video/538114902" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>'),
      },
      {
        text: 'Современные подходы к выбору схем терапии у больных глаукомой. Комбинированная терапия',
        video: formatIframe('<iframe src="https://player.vimeo.com/video/538106918" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>'),
      },
      {
        text: '«Настоящая» клиническая офтальмология: примеры продолжительного наблюдения за пациентами с глаукомой ',
        video: formatIframe('<iframe src="https://player.vimeo.com/video/538117891" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>'),
      },
      {
        text: '«Ступенчатая» терапия глаукомы: принципы и практика',
        img: LectureImg4,
        fallbackImg: LectureFallbackImg4,
      },
      {
        text: 'Недооценность использования в повседневной клинической практике результатов клинико-эпидемиологических (популяционных) исследований в области глаукомы',
        img: LectureImg5,
        fallbackImg: LectureFallbackImg5,
      },
    ],
  },
  {
    authors: [
      {
        name: 'Куроедов Александр Владимирович',
        info: 'д.м.н., начальник офтальмологического отделения ФКУ «ЦВКГ им. Мандрыка» МО РФ, профессор кафедры офтальмологии им. акад. А.П. Нестерова лечебного факультета ФГАОУ ВО РНИМУ им. Пирогова МЗ РФ, член Президиума РГО',
        photo: AuthorImg1,
        fallbackPhoto: AuthorFallbackImg1,
      },
      {
        name: 'Антонов Алексей Анатольевич',
        info: 'ведущий научный сотрудник, к.м.н., ФГБНУ «НИИГБ», Москва',
        photo: AuthorImg2,
        fallbackPhoto: AuthorFallbackImg2,
      },
    ],
    description: 'Курс направлен на всестороннее изучение новой и единственной в России фиксированной комбинации бримонидина и бринзоламида – глазных противоглаукомных капель Симбринза®.',
    isActive: false,
    startMonth: CourseMonthEnum.MAY,
    title: 'Новая ступень эволюции фиксированных комбинаций в лечении глаукомы',
    lectures: [
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg4,
        video: formatIframe('<iframe src="https://player.vimeo.com/video/538106918?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="1920" height="1080" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="010221_Karlova_1_NEW2-MPEG-4.mp4"></iframe>'),
      },
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg5,
      },
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg5,
      },
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg5,
      },
    ],
  },
  {
    authors: [
      {
        name: 'Куроедов Александр Владимирович',
        info: 'д.м.н., начальник офтальмологического отделения ФКУ «ЦВКГ им. Мандрыка» МО РФ, профессор кафедры офтальмологии им. акад. А.П. Нестерова лечебного факультета ФГАОУ ВО РНИМУ им. Пирогова МЗ РФ, член Президиума РГО',
        photo: AuthorImg1,
        fallbackPhoto: AuthorFallbackImg1,
      },
      {
        name: 'Антонов Алексей Анатольевич',
        info: 'ведущий научный сотрудник, к.м.н., ФГБНУ «НИИГБ», Москва',
        photo: AuthorImg2,
        fallbackPhoto: AuthorFallbackImg2,
      },
    ],
    description: 'В курс входят продолжение разговора о приверженности лечению у пациентов с глаукомой, принципы ведения пациентов при сочетании глаукомы и катаракты, алгоритм действий у пациентов с глаукомой низкого давления и рассмотрение клинического случая тяжелого течения глаукомы.',
    isActive: false,
    startMonth: CourseMonthEnum.JUNE,
    title: '«Непростая» глаукома. Выбираем тактику.',
    lectures: [
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg4,
        video: formatIframe('<iframe src="https://player.vimeo.com/video/538106918?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="1920" height="1080" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="010221_Karlova_1_NEW2-MPEG-4.mp4"></iframe>'),
      },
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg5,
      },
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg5,
      },
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg5,
      },
    ],
  },
  {
    authors: [
      {
        name: 'Куроедов Александр Владимирович',
        info: 'д.м.н., начальник офтальмологического отделения ФКУ «ЦВКГ им. Мандрыка» МО РФ, профессор кафедры офтальмологии им. акад. А.П. Нестерова лечебного факультета ФГАОУ ВО РНИМУ им. Пирогова МЗ РФ, член Президиума РГО',
        photo: AuthorImg1,
        fallbackPhoto: AuthorFallbackImg1,
      },
      {
        name: 'Антонов Алексей Анатольевич',
        info: 'ведущий научный сотрудник, к.м.н., ФГБНУ «НИИГБ», Москва',
        photo: AuthorImg2,
        fallbackPhoto: AuthorFallbackImg2,
      },
    ],
    description: 'Коллектив лекторов коснется таких направлений, как длительность работы антиглаукомных гипотензивных препаратов, хирургия глаукомы и принципы перехода к хирургии глаукомы, глаукома и сопутствующие заболевания.',
    isActive: false,
    startMonth: CourseMonthEnum.JUNE,
    title: 'Хирургия глаукомы. Кому, когда и как?',
    lectures: [
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg4,
        video: formatIframe('<iframe src="https://player.vimeo.com/video/538106918?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" width="1920" height="1080" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen title="010221_Karlova_1_NEW2-MPEG-4.mp4"></iframe>'),
      },
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg5,
      },
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg5,
      },
      {
        text: 'Своевременная диагностика и влияние приверженности лечению на прогноз и прогрессирование глаукомной оптической нейропатии (клинический случай)',
        img: LectureImg5,
      },
    ],
  },
];

export default courseList;
