/* eslint-disable react/display-name */
import React from 'react';
import AuthorModal, { AuthorModalData } from '@/components/AuthorModal';
import { ModalTypeEnum } from '@/context/modal';
import ListModal, { ListModalData } from '@/components/ListModal';
import VerifyModal, { VerifyModalData } from '@/components/VerifyModal';
import LectureModal, { LectureModalData } from '@/components/LectureModal';
import MaterialModal, { MaterialModalData } from '@/components/MaterialModal';
import QuestModal, { QuestModalData } from '@/components/QuestModal';
import StatusModal, { StatusModalData } from '@/components/StatusModal';
import InitTestModal, { InitTestModalData } from '@/components/InitTestModal';

export enum ModalSettingModeEnum {
  EDIT = 'edit',
  CREATE = 'create',
}

const MODAL_CONFIG = {
  [ModalTypeEnum.INIT_TEST]: (data: InitTestModalData): JSX.Element => (
    <InitTestModal />
  ),

  [ModalTypeEnum.STATUS]: (data: StatusModalData): JSX.Element => (
    <StatusModal {...data} />
  ),
  [ModalTypeEnum.VERIFY]: (data: VerifyModalData): JSX.Element => (
    <VerifyModal
      {...data}
    />
  ),

  [ModalTypeEnum.COURSE_AUTHOR_LIST]: (data: ListModalData): JSX.Element => (
    <ListModal {...data} />
  ),
  [ModalTypeEnum.COURSE_LECTURE_LIST]: (data: ListModalData): JSX.Element => (
    <ListModal {...data} />
  ),
  [ModalTypeEnum.COURSE_LIST]: (data: ListModalData): JSX.Element => (
    <ListModal {...data} />
  ),
  [ModalTypeEnum.MATERIAL_LIST]: (data: ListModalData): JSX.Element => (
    <ListModal {...data} />
  ),
  [ModalTypeEnum.QUEST_LIST]: (data: ListModalData): JSX.Element => (
    <ListModal {...data} />
  ),

  [ModalTypeEnum.COURSE_AUTHOR_EDIT]: (data: AuthorModalData): JSX.Element => (
    <AuthorModal
      {...data}
      mode={ModalSettingModeEnum.EDIT}
    />
  ),
  [ModalTypeEnum.COURSE_AUTHOR_CREATE]: (data: Partial<AuthorModalData>): JSX.Element => <AuthorModal />,

  [ModalTypeEnum.COURSE_LECTURE_EDIT]: (data: LectureModalData): JSX.Element => (
    <LectureModal
      {...data}
      mode={ModalSettingModeEnum.EDIT}
    />
  ),
  [ModalTypeEnum.COURSE_LECTURE_CREATE]: (data: Partial<LectureModalData>): JSX.Element => <LectureModal />,

  [ModalTypeEnum.MATERIAL_EDIT]: (data: MaterialModalData): JSX.Element => (
    <MaterialModal
      {...data}
      mode={ModalSettingModeEnum.EDIT}
    />
  ),
  [ModalTypeEnum.MATERIAL_CREATE]: (data: Partial<MaterialModalData>): JSX.Element => <MaterialModal />,

  [ModalTypeEnum.QUEST_EDIT]: (data: QuestModalData): JSX.Element => (
    <QuestModal
      {...data}
      mode={ModalSettingModeEnum.EDIT}
    />
  ),
  [ModalTypeEnum.QUEST_CREATE]: (data: Partial<QuestModalData>): JSX.Element => <QuestModal />,
};

export default MODAL_CONFIG;
