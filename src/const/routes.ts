const ROUTES = {
  HOME: '/',
  SIGN_IN: '/sign_in',
  PRIVACY_POLICY: '/privacy-policy',
  ADMIN_COURSE: '/admin/course/:id?',
  QUEST: '/quest/:id?',
  NOT_FOUND: '/404',
};

export default ROUTES;
