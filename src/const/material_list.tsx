import { MaterialData } from '@/types/course';

const materialList: MaterialData[] = [
  {
    title: 'Клинические рекомендации "Глаукома первичная открытоугольная"',
    url: 'http://avo-portal.ru/documents/fkr/KR_POYG_21_05_2020.pdf',
  },
  {
    title: 'Клинические рекомендации "Глаукома первичная закрытоугольная"',
    url: 'http://avo-portal.ru/documents/fkr/KR_PZYG_21_05_2020.pdf',
  },
  {
    title: 'Руководство по лечению глаукомы Международного совета по офтальмологии (МСО)',
    url: 'http://www.icoph.org/downloads/ICOGlaucomaGuideline-Russian.pdf',
  },
  {
    title: '(ENG) Руководство по лечению глаукомы Международного совета по офтальмологии (МСО)',
    url: 'http://www.icoph.org/downloads/ICOGlaucomaGuidelines.pdf',
  },
  {
    title: 'Рекомендации европейского глаукомного общества',
    url: 'https://bjo.bmj.com/content/bjophthalmol/101/4/1.full.pdf',
  },
  {
    title: 'Инструкции по медицинскому применению офтальмологических препаратов "Новартис Фарма"',
    url: 'https://www.novartis.ru/novartisproducts?sort_bef_combine=title+ASC&sort_bef_combine=title+ASC&combine=%D0%9E%D1%84%D1%82%D0%B0%D0%BB%D1%8C%D0%BC%D0%BE%D0%BB%D0%BE%D0%B3%D0%B8%D1%8F ',
  },
  {
    title: 'Диагностика глаукомы. Гониоскопия',
    url: 'https://drive.google.com/file/d/1-T9oOjpnkK4m3Wod7dNYtDN8GNSP0Mth/view?usp=sharing',
  },
  {
    title: 'Диагностика глаукомы. ОКТ сетчатки и зрительного нерва',
    url: 'https://drive.google.com/file/d/18h6dDhDp0Rh3buifNA-91RbyDCLOJtO0/view?usp=sharing',
  },
  {
    title: 'Диагностика глаукомы. Периметрия. Тонометрия',
    url: 'https://drive.google.com/file/d/1vVnWZBAMIA95aKBpsafueYfSkB_NWSz_/view?usp=sharing',
  },
];

export default materialList;
