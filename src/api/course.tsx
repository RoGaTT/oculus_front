/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { CourseMonthEnum } from '@/types/course';
import { CourseModel } from '@/types/models';
import API from '@/utils/functions/api';

export type ICourseCreate = {
  title: string
  description: string
  startMonth: CourseMonthEnum
  isAnounce: boolean,
  lectures: string[]
  authors: string[]
}

class APICourse {
  constructor(private readonly base: API) {}

  async create(data: ICourseCreate) {
    return this.base.post<ICourseCreate, CourseModel>(
      '/course',
      data,
    );
  }

  async update(id: string, data: ICourseCreate) {
    return this.base.patch<ICourseCreate, CourseModel>(
      `/course/${id}`,
      data,
    );
  }

  async getById(id: string) {
    return this.base.get<string, CourseModel>(
      `/course/${id}`,
    );
  }

  async getAll() {
    const list = await this.base.get<Record<string, unknown>, CourseModel[]>(
      '/course',
    );

    return list as CourseModel[] || [];
  }

  async deleteById(id: string) {
    return this.base.delete<string, CourseModel>(
      `/course/${id}`,
    );
  }
}

export default APICourse;
