/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { LectureModel } from '@/types/models';
import API from '@/utils/functions/api';

export type CourseLecture = {
  _id: string;
  text: string;
  video: string;
  photo: string;
  fallbackPhoto: string;
}

export type ILectureCreate = {
  text: string;
  video: string;
  photo?: File,
}

class APILecture {
  constructor(private readonly base: API) {}

  async create(data: ILectureCreate) {
    const formData = new FormData();

    Object.entries(data).forEach(([key, value]) => {
      if (value === undefined) return;

      formData.set(key, value);
    });

    return this.base.post<FormData, LectureModel>(
      '/lecture',
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
  }

  async update(id: string, data: ILectureCreate) {
    const formData = new FormData();

    Object.entries(data).forEach(([key, value]) => {
      if (value === undefined) return;

      formData.set(key, value);
    });

    return this.base.patch<FormData, LectureModel>(
      `/lecture/${id}`,
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
  }

  async getById(id: string) {
    return this.base.get<string, LectureModel>(
      `/lecture/${id}`,
    );
  }

  async getAll() {
    const list = await this.base.get<Record<string, unknown>, LectureModel[]>(
      '/lecture',
    );

    return list as LectureModel[] || [];
  }

  async deleteById(id: string) {
    return this.base.delete<string, LectureModel>(
      `/lecture/${id}`,
    );
  }
}

export default APILecture;
