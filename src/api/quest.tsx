/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { CourseMonthEnum } from '@/types/course';
import { QuestModel } from '@/types/models';
import API from '@/utils/functions/api';

export type IQuestCreate = {
  title: string
  description: string
  url: string
  isAnounce: boolean
  startMonth?: CourseMonthEnum
  photo?: File
}

class APIQuest {
  constructor(private readonly base: API) {}

  async create(data: IQuestCreate) {
    const formData = new FormData();

    Object.entries(data).forEach(([key, value]) => {
      if (value === undefined) return;

      if (typeof value !== 'boolean') formData.set(key, value);
      else formData.set(key, `${value}`);
    });

    return this.base.post<FormData, QuestModel>(
      '/quest',
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
  }

  async update(id: string, data: IQuestCreate) {
    const formData = new FormData();

    Object.entries(data).forEach(([key, value]) => {
      if (value === undefined) return;

      if (typeof value !== 'boolean') formData.set(key, value);
      else formData.set(key, `${value}`);
    });

    return this.base.patch<FormData, QuestModel>(
      `/quest/${id}`,
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
  }

  async getById(id: string) {
    return this.base.get<string, QuestModel>(
      `/quest/${id}`,
    );
  }

  async getAll() {
    const list = await this.base.get<Record<string, unknown>, QuestModel[]>(
      '/quest',
    );

    return list as QuestModel[] || [];
  }

  async deleteById(id: string) {
    return this.base.delete<string, QuestModel>(
      `/quest/${id}`,
    );
  }
}

export default APIQuest;
