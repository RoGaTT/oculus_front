/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { CourseModel, MaterialModel, QuestModel } from '@/types/models';
import API from '@/utils/functions/api';

class APIMetadata {
  constructor(private readonly base: API) {}

  async getMainCourse() {
    const mainCourse = await this.base.get<string, CourseModel>(
      '/metadata/main_course',
    );
    return mainCourse as CourseModel;
  }

  async setMainCourse(id: string) {
    const mainCourse = await this.base.post<{id: string}, CourseModel>(
      '/metadata/main_course',
      { id },
    );
    return mainCourse as CourseModel;
  }

  async getMainQuest() {
    const mainQuest = await this.base.get<string, QuestModel>(
      '/metadata/main_quest',
    );
    return mainQuest as QuestModel;
  }

  async setMainQuest(id: string) {
    const mainQuest = await this.base.post<{id: string}, QuestModel>(
      '/metadata/main_quest',
      { id },
    );
    return mainQuest as QuestModel;
  }

  async getCourses() {
    const courses = await this.base.get<string, CourseModel[]>(
      '/metadata/courses',
    );

    return courses as CourseModel[];
  }

  async setCourses(idList: string[]) {
    const courses = await this.base.post<{ idList: string[] }, CourseModel[]>(
      '/metadata/courses',
      {
        idList,
      },
    );
    return courses as CourseModel[];
  }

  async getQuests() {
    const quests = await this.base.get<string, QuestModel[]>(
      '/metadata/quests',
    );
    return quests as QuestModel[];
  }

  async setQuests(idList: string[]) {
    const quests = await this.base.post<{ idList: string[] }, QuestModel[]>(
      '/metadata/quests',
      {
        idList,
      },
    );
    return quests as QuestModel[];
  }

  async getMaterials() {
    const materials = await this.base.get<string, MaterialModel[]>(
      '/metadata/materials',
    );
    return materials as MaterialModel[];
  }

  async setMaterials(idList: string[]) {
    const materials = await this.base.post<{ idList: string[] }, MaterialModel[]>(
      '/metadata/materials',
      {
        idList,
      },
    );
    return materials as MaterialModel[];
  }
}

export default APIMetadata;
