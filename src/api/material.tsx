/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { MaterialModel } from '@/types/models';
import API from '@/utils/functions/api';

export type IMaterialCreate = {
  title: string;
  file?: File,
  needVerify?: boolean
  url: string
}

class APIMaterial {
  constructor(private readonly base: API) {}

  async create(data: IMaterialCreate) {
    const formData = new FormData();

    Object.entries(data).forEach(([key, value]) => {
      if (value === undefined) return;

      if (typeof value !== 'boolean') formData.set(key, value);
      else formData.set(key, `${value}`);
    });

    return this.base.post<FormData, MaterialModel>(
      '/material',
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
  }

  async update(id: string, data: IMaterialCreate) {
    const formData = new FormData();

    Object.entries(data).forEach(([key, value]) => {
      if (value === undefined) return;

      if (typeof value !== 'boolean') formData.set(key, value);
      else formData.set(key, `${value}`);
    });

    return this.base.patch<FormData, MaterialModel>(
      `/material/${id}`,
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
  }

  async getById(id: string) {
    return this.base.get<string, MaterialModel>(
      `/material/${id}`,
    );
  }

  async getAll() {
    const list = await this.base.get<Record<string, unknown>, MaterialModel[]>(
      '/material',
    );

    return list as MaterialModel[] || [];
  }

  async deleteById(id: string) {
    return this.base.delete<string, MaterialModel>(
      `/material/${id}`,
    );
  }
}

export default APIMaterial;
