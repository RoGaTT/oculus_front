import { AxiosResponse } from 'axios';
import { HttpResponse } from '@/types/http';
import API from '@/utils/functions/api';

export type IAuthLogin = {
  username: string;
  password: string;
}

export interface OAuthLogin {
  accessToken: string;
}

class APIAuth {
  constructor(private readonly base: API) {}

  async login(
    data: IAuthLogin,
  ): Promise<AxiosResponse<OAuthLogin | HttpResponse> | OAuthLogin | HttpResponse | undefined> {
    return this.base.post<IAuthLogin, HttpResponse | OAuthLogin>(
      '/auth/login',
      data,
      {
        onErrorRedirect: false,
      },
    );
  }
}

export default APIAuth;
