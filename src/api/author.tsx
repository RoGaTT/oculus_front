/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { AuthorModel } from '@/types/models';
import API from '@/utils/functions/api';

export type CourseAuthor = {
  _id: string;
  name: string;
  shortName: string;
  info: string;
  photo: string;
  fallbackPhoto: string;
}

export type IAuthorCreate = {
  name: string;
  shortName: string;
  info: string;
  photo?: File,
}

class APIAuthor {
  constructor(private readonly base: API) {}

  async create(data: IAuthorCreate) {
    const formData = new FormData();

    Object.entries(data).forEach(([key, value]) => {
      if (!value) return;

      formData.set(key, value);
    });

    return this.base.post<FormData, AuthorModel>(
      '/author',
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
  }

  async update(id: string, data: IAuthorCreate) {
    const formData = new FormData();

    Object.entries(data).forEach(([key, value]) => {
      if (!value) return;

      if (value) formData.set(key, value);
    });

    return this.base.patch<FormData, AuthorModel>(
      `/author/${id}`,
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
  }

  async getById(id: string) {
    return this.base.get<string, AuthorModel>(
      `/author/${id}`,
    );
  }

  async getAll() {
    const list = await this.base.get<Record<string, unknown>, AuthorModel[]>(
      '/author',
    );

    return list as AuthorModel[] || [];
  }

  async deleteById(id: string) {
    return this.base.delete<string, AuthorModel>(
      `/author/${id}`,
    );
  }
}

export default APIAuthor;
