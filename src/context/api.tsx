/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import APIAuth from '@/api/auth';
import APIAuthor from '@/api/author';
import APICourse from '@/api/course';
import APILecture from '@/api/lecture';
import APIMaterial from '@/api/material';
import APIMetadata from '@/api/metadata';
import APIQuest from '@/api/quest';
import API from '@/utils/functions/api';

type APIContextType = {
  auth: APIAuth,
  base: API,
  author: APIAuthor,
  lecture: APILecture,
  course: APICourse
  material: APIMaterial,
  quest: APIQuest,
  metadata: APIMetadata,
}

const APIContext = React.createContext<APIContextType>({
  auth: {} as APIAuth,
  base: {} as API,
  author: {} as APIAuthor,
  lecture: {} as APILecture,
  course: {} as APICourse,
  quest: {} as APIQuest,
  material: {} as APIMaterial,
  metadata: {} as APIMetadata,

});

export default APIContext;
