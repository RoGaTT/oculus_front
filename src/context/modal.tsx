/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';

export enum ModalTypeEnum {
  COURSE_AUTHOR_LIST = 'course_author_list',
  COURSE_AUTHOR_CREATE = 'course_author_create',
  COURSE_AUTHOR_EDIT = 'course_author_edit',
  COURSE_LECTURE_LIST = 'course_lecture_list',
  COURSE_LECTURE_CREATE = 'course_lecture_create',
  COURSE_LECTURE_EDIT = 'course_lecture_edit',
  COURSE_LIST = 'course_list',
  COURSE_CREATE = 'course_create',
  COURSE_EDIT = 'course_edit',
  QUEST_LIST = 'quest_list',
  QUEST_CREATE = 'quest_create',
  QUEST_EDIT = 'quest_edit',
  MATERIAL_LIST = 'material_list',
  MATERIAL_CREATE = 'material_create',
  MATERIAL_EDIT = 'material_edit',
  VERIFY = 'verify',
  STATUS = 'status',
  INIT_TEST = 'init_test',
  INIT_TEST_FAILURE = 'init_test_failure'
}

export type ModalTypeOptions = {
  isClosingDisabled?: boolean,
  classes?: { [key: string]: string },
  maxWidth?: 'lg' | 'md' | 'sm' | 'xl' | 'xs' | false;
}

export type ModalType = ModalTypeOptions & {
  type: ModalTypeEnum,
  data: unknown,
  onClose?: { (): void },
}

type ModalContextType = {
  modalList: ModalType[],
  openModal: { (type: ModalTypeEnum, data?: unknown, onClose?: { (): void }): () => void },
  closeModal: { (type: ModalTypeEnum): () => void },
  closeAllModal: { (): () => void }
}

const ModalContext = React.createContext<ModalContextType>({
  modalList: [],
  openModal: () => () => null,
  closeModal: () => () => null,
  closeAllModal: () => () => null,
});

export default ModalContext;
