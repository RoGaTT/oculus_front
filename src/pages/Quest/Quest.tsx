import React, { FC, useEffect, useMemo } from 'react';
import { useCookies } from 'react-cookie';
import { v4 as uuidv4 } from 'uuid';
import { useParams } from 'react-router';
import Footer from '@/utils/components/Layout/components/Footer';
import { CookiesMapEnum } from '@/types/config';
import { getIKSQuestLink } from '@/utils/functions/iks';
import { IKSCookiesOptions } from '@/const/cookies';
import classes from './Quest.module.scss';

type PropsType = {

}

type ParamsType = {
  id?: string
}

const Quest: FC<PropsType> = () => {
  const [cookies, setCookie] = useCookies([
    CookiesMapEnum.IKS_USER_TOKEN,
  ]);

  const params = useParams<ParamsType>();

  useEffect(() => {
    let token = cookies[CookiesMapEnum.IKS_USER_TOKEN];
    if (!token) {
      token = uuidv4();
      setCookie(CookiesMapEnum.IKS_USER_TOKEN, token, IKSCookiesOptions);
    }
  }, [cookies, setCookie]);

  const memoQuestLink = useMemo(() => getIKSQuestLink(cookies[CookiesMapEnum.IKS_USER_TOKEN], params.id || '2'), [cookies, params.id]);

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <iframe
          title="Quest"
          src={memoQuestLink}
          frameBorder="0"
          allow=""
          allowFullScreen
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            width: '100%',
            height: '100%',
            border: 'none',
            margin: 0,
            padding: 0,
            overflow: 'hidden',
            zIndex: 999999,
          }}
        />
      </div>
      <Footer noSubscribe />
    </div>
  );
};

export default Quest;
