import React, { FC } from 'react';
import { Helmet } from 'react-helmet-async';
import Quest from './Quest';

const QuestPage: FC = () => (
  <>
    <Helmet title="Quest page" />
    <Quest />
  </>
);

export default QuestPage;
