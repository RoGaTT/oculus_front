import React, { FC } from 'react';
import { Helmet } from 'react-helmet-async';
import PrivacyPolicy from './PrivacyPolicy';

const PrivacyPolicyPage: FC = () => (
  <>
    <Helmet title="Политика конфиденциальности" />
    <PrivacyPolicy />
  </>
);

export default PrivacyPolicyPage;
