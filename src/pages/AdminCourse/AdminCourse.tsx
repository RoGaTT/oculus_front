import clsx from 'clsx';
import _ from 'lodash';
import React, {
  createRef,
  FC, useCallback, useContext, useEffect, useMemo, useState,
} from 'react';
import { useParams } from 'react-router';
import AuthorCard from '@/components/AuthorCard';
import LectureCard from '@/components/LectureCard';
import {
  CourseMonthEnum,
} from '@/types/course';
import Button from '@/ui/Button';
import Container from '@/utils/components/Container';
import CrossIcon from '@/assets/img/cross.svg';
import ModalContext, { ModalTypeEnum } from '@/context/modal';
import APIContext from '@/context/api';
import { ICourseCreate } from '@/api/course';
import { AuthorModel, CourseModel, LectureModel } from '@/types/models';
import { removeBrFromText } from '@/utils/functions/text';
import classes from './AdminCourse.module.scss';

type PropsType = {

}

type ParamsType = {
  id?: string,
}

const AdminCourse: FC<PropsType> = () => {
  const authorSelectRef = createRef<HTMLSelectElement>();
  const lectureSelectRef = createRef<HTMLSelectElement>();

  const [isMain, setMainState] = useState<boolean>(false);

  const { id } = useParams<ParamsType>();

  const api = useContext(APIContext);

  const [values, setValues] = useState<ICourseCreate>({
    title: '',
    description: '',
    isAnounce: false,
    startMonth: CourseMonthEnum.APRIL,
    lectures: [],
    authors: [],
  });

  const [allAuthors, setAllAuthors] = useState<AuthorModel[]>([]);
  const [authors, setAuthors] = useState<AuthorModel[]>([]);
  const [allLectures, setAllLectures] = useState<LectureModel[]>([]);
  const [lectures, setLectures] = useState<LectureModel[]>([]);

  const { openModal } = useContext(ModalContext);

  useEffect(() => {
    if (id) {
      api.course.getById(id).then((course) => {
        const data = course as CourseModel;
        setValues({
          ...values,
          title: data.title,
          description: data.description,
          startMonth: data.startMonth || CourseMonthEnum.JANUARY,
          isAnounce: data.isAnounce,
        });
        setAuthors(data.authors.map((el) => el.data));
        setLectures(data.lectures.map((el) => el.data));
      });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [api.course]);

  useEffect(() => {
    if (id) {
      api.metadata.getMainCourse().then((data) => {
        setMainState(data._id === id);
      });
    }
  }, [api.metadata, id]);

  const memoAllAuthors = useMemo(
    () => allAuthors.filter((author) => !authors.find((el) => el._id === author._id)), [allAuthors, authors],
  );

  const memoAllLectures = useMemo(
    () => allLectures.filter((lecture) => !lectures.find((el) => el._id === lecture._id)), [allLectures, lectures],
  );

  const handleOnSubmit = useCallback(onSubmit, [api.course, api.metadata, authors, id, lectures, openModal, values]);
  const handleOnInputChange = useCallback(onInputChange, [values]);
  const handleAddAuthor = useCallback(addAuthor, [authorSelectRef, authors, memoAllAuthors]);
  const handleRemoveAuthorFromList = useCallback(removeAuthorFromList, [authors]);
  const handleAddLecture = useCallback(addLecture, [lectureSelectRef, lectures, memoAllLectures]);
  const handleOnLectureMove = useCallback(onLectureMove, [lectures]);
  const handleRemoveLectureFromList = useCallback(removeLectureFromList, [lectures]);
  const handleFetchAllAuthors = useCallback(fetchAllAuthors, [api.author]);
  const handleFetchAllLectures = useCallback(fetchAllLectures, [api.lecture]);

  useEffect(() => {
    handleFetchAllAuthors();
  }, [handleFetchAllAuthors]);

  useEffect(() => {
    handleFetchAllLectures();
  }, [handleFetchAllLectures]);

  return (
    <Container className={classes.root}>
      <div className={classes.head}>
        <h3>Библиотека курсов – Администрирование</h3>
        <Button onClick={handleOnSubmit}>Сохранить</Button>
      </div>
      <h2>{values.title ? removeBrFromText(values.title) : 'Нет названия курса'}</h2>
      <form
        className={classes.form}
      >
        <div className={classes.block}>
          <div className={classes['block-title']}>
            <div className={classes['block-title__head']}>
              <label className={classes.required} htmlFor="title">
                Название курса
              </label>
              <span>
                {values.title.length}
                /80
              </span>
            </div>
            <textarea
              maxLength={80}
              rows={Math.ceil(values.title.length / 59)}
              onChange={handleOnInputChange('title')}
              value={values.title}
            />
          </div>
          <div className={classes['block-anounce']}>
            <div className={classes['block-anounce__head']}>
              <h4>Анонс</h4>
            </div>
            <div className={classes['block-anounce__content']}>
              <label className={clsx(values.isAnounce && classes.isChecked)}>
                <input
                  type="checkbox"
                  onChange={onInputChange('isAnounce')}
                  checked={values.isAnounce}
                />
              </label>
              <label htmlFor="startMonth">Анонс курса</label>
              <select
                id="startMonth"
                disabled={!values.isAnounce}
                onChange={handleOnInputChange('startMonth')}
                placeholder="Выберите месяц"
                value={values.startMonth}
              >
                {
                  Object.entries(CourseMonthEnum).map(([key, value]) => (
                    <option value={value} key={key}>{_.upperFirst(value)}</option>
                  ))
                }
              </select>
            </div>
            <div className={classes.isMain}>
              <label className={clsx(isMain && classes.isChecked)}>
                <input
                  type="checkbox"
                  onChange={(e) => {
                    setMainState(e.target.checked);
                  }}
                  checked={isMain}
                />
              </label>
              <label htmlFor="isMain">Сделать Главным</label>
            </div>
          </div>
          <div className={classes['block-list']}>
            <div className={classes['block-list__head']}>
              <h4>Лекторы</h4>
            </div>
            <div className={classes['block-list__content-controls']}>
              <Button onClick={
                openModal(ModalTypeEnum.COURSE_AUTHOR_CREATE, {
                  onDelete: handleFetchAllAuthors,
                }, handleFetchAllAuthors)
              }
              >
                Добавить лектора
              </Button>
            </div>
            <div className={classes['block-list__content']}>
              <div className={clsx(classes['block-list__content-input'])}>
                <select
                  id="authors"
                  ref={authorSelectRef}
                  onChange={handleAddAuthor}
                  disabled={!memoAllAuthors.length}
                  defaultValue=""
                >
                  <option value="" disabled hidden>Выберите лектора</option>
                  {
                    memoAllAuthors.map((author) => (
                      <option
                        value={author._id}
                        key={author._id}
                        className={classes.selectOption}
                      >
                        {author.name}
                      </option>
                    ))
                  }
                </select>
                <Button onClick={() => setAuthors([])}>
                  <img src={CrossIcon} alt="" />
                </Button>
              </div>
              <div className={clsx(classes['block-list__content-list'], classes.active)}>
                {
                  authors.filter((item) => !!allAuthors.find((el) => el._id === item._id)).map((author) => (
                    <AuthorCard
                      author={author}
                      key={author._id}
                      onDelete={handleRemoveAuthorFromList(author._id)}
                      onEdit={openModal(ModalTypeEnum.COURSE_AUTHOR_EDIT, {
                        id: author._id,
                        onDelete: handleFetchAllAuthors,
                      })}
                    />
                  ))
                }
              </div>
            </div>
          </div>
        </div>
        <div className={classes.block}>
          <div className={classes['block-description']}>
            <div className={classes['block-description__head']}>
              <label className={classes.required} htmlFor="description">
                Описание курса
              </label>
              <span>
                {values.description.length}
                /312
              </span>
            </div>
            <textarea
              maxLength={312}
              id="description"
              rows={Math.ceil(values.description.length / 59)}
              onChange={handleOnInputChange('description')}
              value={values.description}
            />
          </div>
          <div className={classes['block-list']}>
            <div className={classes['block-list__head']}>
              <h4>Лекции</h4>
            </div>
            <div className={classes['block-list__content-controls']}>
              <Button onClick={
                openModal(ModalTypeEnum.COURSE_LECTURE_CREATE, {
                  onDelete: handleFetchAllLectures,
                }, handleFetchAllLectures)
              }
              >
                Добавить лекцию
              </Button>
            </div>
            <div className={classes['block-list__content']}>
              <div className={clsx(classes['block-list__content-input'])}>
                <select
                  id="authors"
                  ref={lectureSelectRef}
                  onChange={handleAddLecture}
                  disabled={!memoAllLectures.length}
                  defaultValue=""
                >
                  <option value="" disabled hidden>Выберите лекцию</option>
                  {
                    memoAllLectures.map((lecture) => (
                      <option
                        value={lecture._id}
                        key={lecture._id}
                        className={classes.selectOption}
                      >
                        {lecture.text}
                      </option>
                    ))
                  }
                </select>
                <Button onClick={() => setLectures([])}>
                  <img src={CrossIcon} alt="" />
                </Button>
              </div>
              <div className={clsx(classes['block-list__content-list'], classes.active)}>
                {
                  lectures
                    .filter((item) => !!allLectures.find((el) => el._id === item._id)).map((lecture, lectureIndex) => (
                      <LectureCard
                        index={lectureIndex}
                        lecture={lecture}
                        key={lecture._id}
                        isFirst={!lectureIndex}
                        isLast={lectureIndex + 1 === lectures.length}
                        onDelete={handleRemoveLectureFromList(lecture._id)}
                        onEdit={openModal(ModalTypeEnum.COURSE_LECTURE_EDIT, {
                          id: lecture._id,
                          onDelete: handleFetchAllLectures,
                        })}
                        onMove={handleOnLectureMove(lecture._id)}
                      />
                    ))
                }
              </div>
            </div>
          </div>
        </div>
      </form>
    </Container>
  );

  async function fetchAllAuthors() {
    const allAuthors = await api.author.getAll();

    setAllAuthors(allAuthors);
  }
  async function fetchAllLectures() {
    const allLectures = await api.lecture.getAll();

    setAllLectures(allLectures);
  }

  function onInputChange(key: keyof ICourseCreate) {
    return (
      e: React.ChangeEvent<HTMLTextAreaElement> |
          React.ChangeEvent<HTMLSelectElement> |
          React.ChangeEvent<HTMLInputElement>,
    ) => {
      if (key === 'isAnounce') {
        setValues({
          ...values,
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          isAnounce: (e.target as any)?.checked,
        });
      } else {
        setValues({
          ...values,
          [key]: e.target.value,
        });
      }
    };
  }

  function onLectureMove(id: string) {
    return (direction: 'down' | 'up') => () => {
      const listCopy = [...lectures];
      const elIndex = listCopy.findIndex((el) => el._id === id);
      if (elIndex === -1) return;
      const deletedItem = listCopy.splice(elIndex, 1);
      listCopy.splice(elIndex + (direction === 'down' ? 1 : -1), 0, deletedItem[0]);
      setLectures(listCopy);
    };
  }

  async function onSubmit() {
    const data: ICourseCreate = {
      ...values,
      lectures: lectures.map((el) => el._id),
      authors: authors.map((el) => el._id),
    };
    if (!id) {
      const res = (await api.course.create(data)) as CourseModel;
      await api.metadata.setMainCourse(res._id);
    }
    if (id) {
      await api.course.update(id, data);
      await api.metadata.setMainCourse(id);
    }
    openModal(ModalTypeEnum.STATUS)();
  }

  function addAuthor(e: React.ChangeEvent<HTMLSelectElement>) {
    const { value } = e.target;
    if (value === '') return;
    if (authorSelectRef.current) authorSelectRef.current.value = '';

    if (!authors.find((el) => value === el._id)) {
      const author = memoAllAuthors.find((author) => author._id === value);
      if (author) {
        setAuthors([
          ...authors,
          author,
        ]);
      }
    }
  }
  function removeAuthorFromList(id: string) {
    return () => {
      setAuthors(authors.filter((el) => el._id !== id));
    };
  }

  function addLecture(e: React.ChangeEvent<HTMLSelectElement>) {
    const { value } = e.target;
    if (value === '') return;
    if (lectureSelectRef.current) lectureSelectRef.current.value = '';

    if (!lectures.find((el) => value === el._id)) {
      const lecture = memoAllLectures.find((lecture) => lecture._id === value);
      if (lecture) {
        setLectures([
          ...lectures,
          lecture,
        ]);
      }
    }
  }
  function removeLectureFromList(id: string) {
    return () => {
      setLectures(lectures.filter((el) => el._id !== id));
    };
  }
};

export default AdminCourse;
