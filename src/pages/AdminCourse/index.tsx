import React, { FC } from 'react';
import { Helmet } from 'react-helmet-async';
import AdminCourse from './AdminCourse';

const AdminCoursePage: FC = () => (
  <>
    <Helmet title="Admin course" />
    <AdminCourse />
  </>
);

export default AdminCoursePage;
