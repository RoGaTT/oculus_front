import React, {
  ChangeEvent, FC, useCallback, useContext, useEffect, useState,
} from 'react';
import { useCookies } from 'react-cookie';
import { useHistory } from 'react-router';
import { IAuthLogin, OAuthLogin } from '@/api/auth';
import APIContext from '@/context/api';
import { CookiesMapEnum } from '@/types/config';
import Button from '@/ui/Button';
import classes from './SignIn.module.scss';

type PropsType = {

}

const SignIn: FC<PropsType> = () => {
  const [data, setData] = useState<IAuthLogin>({
    username: '',
    password: '',
  });

  const [cookies, setCookie] = useCookies([
    CookiesMapEnum.BEARER_AUTH_TOKEN,
  ]);
  const history = useHistory();

  const api = useContext(APIContext);

  const handleOnInputChange = useCallback(onInputChange, [data]);
  const handleOnSubmit = useCallback(onSubmit, [api.auth, data, history, setCookie]);

  useEffect(() => {
    if (cookies[CookiesMapEnum.BEARER_AUTH_TOKEN]) history.replace('/');
  }, [cookies, history]);

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <h2>Вход</h2>
        <label>
          <span>
            Логин
            {' '}
            <span>*</span>
          </span>
          <input type="text" placeholder="Введите имя пользователя" onChange={handleOnInputChange('username')} />
        </label>
        <label>
          <span>
            Пользователя
            {' '}
            <span>*</span>
          </span>
          <input type="password" placeholder="Введите пароль" onChange={handleOnInputChange('password')} />
        </label>
        <Button onClick={handleOnSubmit}>Войти</Button>
      </div>
    </div>
  );

  function onInputChange(key: keyof IAuthLogin) {
    return (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>) => {
      setData({
        ...data,
        [key]: e.target.value,
      });
    };
  }

  async function onSubmit() {
    api.auth.login(data).then((res) => {
      const data = (res as OAuthLogin);
      if (data?.accessToken && data?.accessToken !== 'undefined') {
        setCookie(CookiesMapEnum.BEARER_AUTH_TOKEN, data.accessToken);
      }
      history.replace('/');
    });
  }
};

export default SignIn;
