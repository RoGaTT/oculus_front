/* eslint-disable react/no-danger */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/no-array-index-key */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable max-len */
import clsx from 'clsx';
import React, {
  FC, useCallback, useContext, useEffect, useMemo, useRef, useState,
} from 'react';
import { v4 as uuidv4 } from 'uuid';
import SwipeSlider from 'react-swipe';
import { useHistory } from 'react-router-dom';
import { useCookies } from 'react-cookie';
import Button from '@/ui/Button';
import Container from '@/utils/components/Container';

import FlagImg from '@/assets/img/library_flag.svg';

import ArrowShowAllIcon from '@/assets/img/show_all_arrow.webp';

import ArrowShowAllFallbackIcon from '@/assets/img/fallback/show_all_arrow.png';

import MainDesktopImg from '@/assets/img/main_desktop.svg';
import MainPersonImg from '@/assets/img/main_person.svg';
import ArrowSliderIcon from '@/assets/img/slider_arrow.svg';
import EditIcon from '@/assets/img/edit_icon.svg';
import VerticalFlag from '@/assets/img/fallback/vertical_flag.png';
import MaterialIcon from '@/assets/img/material_icon.svg';
import CourseCard from '@/components/CourseCard';
import { goBlank, goWithScroll } from '@/utils/functions/dom';
import ModalContext, { ModalTypeEnum } from '@/context/modal';
import APIContext from '@/context/api';
import { ListModalData } from '@/components/ListModal';
import { CourseModel, MaterialModel, QuestModel } from '@/types/models';
import { getStaticPath } from '@/utils/functions/path';
import Footer from '@/utils/components/Layout/components/Footer';
import { CookiesMapEnum } from '@/types/config';
// import { getIKSQuestLink } from '@/utils/functions/iks';
import { IKSCookiesOptions } from '@/const/cookies';
import classes from './Home.module.scss';

interface ArrowPropsType {
  onClick: { (): void },
  isNext?: boolean,
}

const Arrow: FC<ArrowPropsType> = ({
  isNext,
  onClick,
}) => (
  <div className={clsx(classes['quest-videos__slider-arrow__wrapper'], isNext && classes.next)}>
    <button
      type="button"
      onClick={onClick}
      className={classes['quest-videos__slider-arrow']}
    >
      <img src={ArrowSliderIcon} alt="" />
    </button>
  </div>
);

type PropsType = {

}

const Home: FC<PropsType> = () => {
  const [openedCourseIndex, setOpenedCourseIndex] = useState<number | null>(null);
  const [areMaterialsOpened, setMaterialsOpenState] = useState<boolean>(false);
  const [isAdminPanelActive, setAdminPanelState] = useState<boolean>(false);

  const questSliderRef = useRef<SwipeSlider | null>(null);
  const handleOnArrowClick = useCallback(onArrowClick, []);

  const { openModal, closeModal } = useContext(ModalContext);
  const api = useContext(APIContext);
  const history = useHistory();
  const [cookies, setCookie] = useCookies([
    CookiesMapEnum.IKS_USER_TOKEN,
  ]);

  useEffect(() => {
    setAdminPanelState(api.base.isToken());
  }, [api.base, api.base.isToken]);

  const [mainCourse, setMainCourse] = useState<CourseModel>();
  const [mainQuest, setMainQuest] = useState<QuestModel>();
  const [courseList, setCourseList] = useState<CourseModel[]>([]);
  const [questList, setQuestList] = useState<QuestModel[]>([]);
  const [materialList, setMaterialList] = useState<MaterialModel[]>([]);

  const memoFetchCourses = useCallback(fetchCourses, [api.metadata]);
  const memoFetchQuests = useCallback(fetchQuests, [api.metadata]);
  const memoFetchMaterials = useCallback(fetchMaterials, [api.metadata]);

  const memoAdminConfig = useMemo<Record<string, ListModalData>>(() => ({
    courses: {
      formatData: (data: CourseModel[]) => data.map((el) => ({
        id: el._id,
        text: el.title,
        isActive: !!courseList.find((course) => course._id === el._id),
      })),
      title: 'Курсы',
      onRefetch: () => api.course.getAll(),
      onEdit: (id) => {
        closeModal(ModalTypeEnum.COURSE_LIST)();
        history.push(`/admin/course/${id}`);
      },
      onDelete: (id) => api.course.deleteById(id),
      onAdd: () => {
        closeModal(ModalTypeEnum.COURSE_LIST)();
        history.push('/admin/course');
      },
      onSave: async (idList) => {
        await api.metadata.setCourses(idList);
        await memoFetchCourses();
      },
    },
    quests: {
      formatData: (data: QuestModel[]) => data.map((el, elIndex) => ({
        id: el._id,
        text: el.title,
        isActive: !!questList.find((quest) => quest._id === el._id),
      })),
      title: 'Квесты',
      onRefetch: () => api.quest.getAll(),
      onEdit: (id) => {
        closeModal(ModalTypeEnum.QUEST_LIST)();
        openModal(ModalTypeEnum.QUEST_EDIT, {
          id,
        })();
      },
      onDelete: (id) => api.quest.deleteById(id),
      onAdd: () => {
        closeModal(ModalTypeEnum.QUEST_LIST)();
        openModal(ModalTypeEnum.QUEST_CREATE)();
      },
      onSave: async (idList) => {
        await api.metadata.setQuests(idList);
        await memoFetchQuests();
      },
    },
    materials: {
      formatData: (data: MaterialModel[]) => data.map((el, elIndex) => ({
        id: el._id,
        text: el.title,
        isActive: !!materialList.find((material) => material._id === el._id),
      })),
      title: 'Материалы',
      onRefetch: () => api.material.getAll(),
      onEdit: (id) => {
        closeModal(ModalTypeEnum.MATERIAL_LIST)();
        openModal(ModalTypeEnum.MATERIAL_EDIT, {
          id,
        })();
      },
      onDelete: (id) => api.material.deleteById(id),
      onAdd: () => {
        closeModal(ModalTypeEnum.MATERIAL_LIST)();
        openModal(ModalTypeEnum.MATERIAL_CREATE)();
      },
      onSave: async (idList) => {
        await api.metadata.setMaterials(idList);
        await memoFetchMaterials();
      },
    },
  }), [api.course, api.material, api.metadata, api.quest, closeModal, courseList, history, materialList, memoFetchCourses, memoFetchMaterials, memoFetchQuests, openModal, questList]);

  useEffect(() => {
    memoFetchCourses();
  }, [memoFetchCourses]);

  useEffect(() => {
    memoFetchQuests();
  }, [memoFetchQuests]);

  useEffect(() => {
    memoFetchMaterials();
  }, [memoFetchMaterials]);

  useEffect(() => {
    api.metadata.getMainCourse().then(setMainCourse);
  }, [api.metadata, api.metadata.getMainCourse]);

  useEffect(() => {
    api.metadata.getMainQuest().then(setMainQuest);
  }, [api.metadata, api.metadata.getMainQuest]);

  const goOuter = useCallback((url: string, needVerify = false) => () => {
    if (needVerify) {
      openModal(ModalTypeEnum.VERIFY, {
        title: 'Вы переходите на внешний сайт, нажмите Принять для подтверждения',
        onAgree: () => {
          closeModal(ModalTypeEnum.VERIFY)();
          goBlank(url)();
        },
      })();
    } else {
      goBlank(url)();
    }
  }, [closeModal, openModal]);

  const memoOnQuestClick = useCallback(onQuestClick, [cookies, history, setCookie]);
  const MemoQuestSliderComponent = useCallback(QuestSliderComponent, [handleOnArrowClick, memoOnQuestClick, questList]);

  return (
    <div className={classes.root}>
      <Container className={classes.main} wrapperClassName={classes['main-wrapper']}>
        <div className={clsx(classes['main-block'], classes.big)}>
          <div className={clsx(
            classes['main-block__content'],
            classes['main-block__content-big'],
          )}
          >
            <h2>Симбринза®</h2>
            <p>
              открывает новые возможности контроля ВГД
              <br />
              {' '}
              у пациентов с сердечно-сосудистыми заболеваниями
            </p>
            <Button
              className={classes['main-block__content-buttonBig']}
              onClick={goOuter('https://oculus-med.ru/about')}
            >
              Узнать больше о препарате
            </Button>
          </div>
          <div className={classes['main-block__content-line']} />
        </div>
        <div className={classes['main-block__container']}>
          <div className={classes['main-block__wrapper']}>
            <div className={classes['main-block']}>
              <div className={clsx(
                classes['main-block__content'],
                classes['main-block__content-small'],
                classes['main-block__content-small__1'],
              )}
              >
                <h3>Новый курс</h3>
                <div className={clsx(
                  classes['main-block__content-small__postTitle'],
                  classes['main-block__content-small__postTitle-1'],
                )}
                >
                  <h4 dangerouslySetInnerHTML={{ __html: mainCourse?.title || 'N/A' }} />
                </div>
                <div className={classes['main-block__content-small__authors']}>
                  <p>Курс читают:</p>
                  {
                    mainCourse?.authors?.sort().slice(0, 4).map((author) => (
                      <span key={author._id}>{author.data.shortName || author.data.name.slice(0, 14)}</span>
                    ))
                  }
                </div>
                <img src={MainDesktopImg} alt="" />
              </div>
              <Button
                className={classes['main-block__footer']}
                onClick={goWithScroll('/', 'courses', history)}
              >
                Начать обучение
              </Button>
            </div>
          </div>
          <div className={classes['main-block__wrapper']}>
            <div className={classes['main-block']}>
              <div className={clsx(
                classes['main-block__content'],
                classes['main-block__content-small'],
                classes['main-block__content-small__2'],
              )}
              >
                {
                  mainQuest?.isAnounce && mainQuest?.startMonth && (
                    <div className={classes.anounce}>
                      <span>
                        старт
                        {' '}
                        {mainQuest.startMonth}
                      </span>
                      <img src={VerticalFlag} alt="" />
                    </div>
                  )
                }
                <h3>Интерактивный квест</h3>
                <div className={clsx(
                  classes['main-block__content-small__postTitle'],
                  classes['main-block__content-small__postTitle-2'],
                )}
                >
                  <h4 dangerouslySetInnerHTML={{ __html: mainQuest?.title || 'N/A' }} />
                </div>
                <img src={MainPersonImg} alt="" />
                <p dangerouslySetInnerHTML={{
                  __html: mainQuest?.description || 'N/A',
                }}
                />
              </div>
              <Button
                className={classes['main-block__footer']}
                onClick={goWithScroll('/', 'quests', history)}
              >
                Пройти квест
              </Button>
            </div>
          </div>
        </div>
      </Container>
      <Container
        className={classes.library}
        id="courses"
        wrapperClassName={classes['library-wrapper']}
      >
        <h2>
          Библиотека курсов
          {
            isAdminPanelActive && (
              <>
                {' '}
                <span
                  className={classes.editButton}
                  onClick={
                    openModal(
                      ModalTypeEnum.COURSE_AUTHOR_LIST,
                      memoAdminConfig.courses,
                    )
                  }
                >
                  <img src={EditIcon} alt="" />
                </span>
              </>
            )
          }
        </h2>
        {
          courseList.map((course, courseIndex) => (
            <CourseCard
              key={course._id}
              onOpen={() => (openedCourseIndex === courseIndex ? setOpenedCourseIndex(null) : setOpenedCourseIndex(courseIndex))}
              isOpened={openedCourseIndex === courseIndex}
              {...course}
            />
          ))
        }
      </Container>
      <Container
        id="quests"
        className={classes.quest}
        wrapperClassName={classes['quest-wrapper']}
      >
        <h3>
          Интерактивные Квесты
          {' '}
          {
            isAdminPanelActive && (
              <>
                {' '}
                <span
                  className={classes.editButton}
                  onClick={
                    openModal(
                      ModalTypeEnum.QUEST_LIST,
                      memoAdminConfig.quests,
                    )
                  }
                >
                  <img src={EditIcon} alt="" />
                </span>
              </>
            )
          }
        </h3>
        <p>
          {' '}
          Интерактивный формат поможет вам пройти различные сценарии развития заболевания
          <br />
          {' '}
          в зависимости от вашего выбора и достичь лучшего результата.
        </p>
        <MemoQuestSliderComponent />
        <div className={classes['quest-materials']} id="materials">
          <h3>
            Полезные материалы
            {' '}
            {
              isAdminPanelActive && (
                <>
                  {' '}
                  <span
                    className={classes.editButton}
                    onClick={
                      openModal(
                        ModalTypeEnum.MATERIAL_LIST,
                        memoAdminConfig.materials,
                      )
                    }
                  >
                    <img src={EditIcon} alt="" />
                  </span>
                </>
              )
            }
          </h3>
          <p>Клинические рекомендации, инструкции по применению.</p>
          {
            materialList.slice(0, 3).map((material, materialIndex) => (
              <div className={classes['quest-materials__item']} key={`material__${materialIndex}`}>
                <img src={MaterialIcon} alt="" />
                <div>
                  <p>{material.title}</p>
                  <button
                    type="button"
                    id={`material_${materialIndex}`}
                    onClick={goOuter(material.url || (material.file && getStaticPath(material.file)) || '/', material.url && material.needVerify)}
                  >
                    Открыть материал
                  </button>
                </div>
              </div>
            ))
          }
          {
            areMaterialsOpened && (
              materialList.slice(3).map((material, materialIndex) => (
                <div className={classes['quest-materials__item']} key={`material__${materialIndex}`}>
                  <img src={MaterialIcon} alt="" />
                  <div>
                    <p>{material.title}</p>
                    <button
                      type="button"
                      onClick={
                        goOuter(material.url || (material.file && getStaticPath(material.file)) || '/', material.url && material.needVerify)
                      }
                    >
                      Открыть материал
                    </button>
                  </div>
                </div>
              ))
            )
          }
          {
            materialList.length > 3 && (
              <Button
                className={areMaterialsOpened ? classes.opened : ''}
                onClick={() => setMaterialsOpenState(!areMaterialsOpened)}
              >
                <span>
                  {
                    areMaterialsOpened ? 'Свернуть всё' : 'Показать всё'
                  }
                </span>
                <picture>
                  <source srcSet={ArrowShowAllIcon} type="image/webp" />
                  <img src={ArrowShowAllFallbackIcon} alt="" />
                </picture>
              </Button>
            )
          }
        </div>
      </Container>
      <Footer />
    </div>
  );

  function fetchCourses() {
    api.metadata.getCourses().then((data) => {
      setCourseList(data);
    });
  }
  function fetchQuests() {
    api.metadata.getQuests().then((data) => {
      setQuestList(data);
    });
  }
  function fetchMaterials() {
    api.metadata.getMaterials().then((data) => {
      setMaterialList(data);
    });
  }

  function onArrowClick(isNext = false): () => void {
    return () => {
      // const { current: slider } = questSliderRef;
      if (!questSliderRef.current) return;
      if (isNext) questSliderRef.current.next();
      else questSliderRef.current.prev();
      questSliderRef.current.getPos();
      // questSliderRef.current.slide(activeQuestSlide + 1, 200);
    };
  }

  function onQuestClick(questId: string, isAnounce: boolean) {
    return () => {
      if (isAnounce) return;
      let token = cookies[CookiesMapEnum.IKS_USER_TOKEN];
      if (!token) {
        token = uuidv4();
        setCookie(CookiesMapEnum.IKS_USER_TOKEN, token, IKSCookiesOptions);
      }
      // const link = getIKSQuestLink(token);
      // goOuter(link)();
      history.push(`/quest/${questId}`);
    };
  }

  function QuestSliderComponent() {
    return (
      <div className={classes['quest-videos']}>
        <Arrow onClick={handleOnArrowClick(false)} />
        <div className={classes['quest-videos__slider-wrapper']}>
          <SwipeSlider
            childCount={2}
            ref={(el) => { questSliderRef.current = el; }}
            className={classes['quest-videos__slider']}
            swipeOptions={{
              speed: 500,
              stopPropagation: true,
              // callback: () => setImmediate(() => questSliderRef.current && setActiveQuestSlide(questSliderRef.current.getPos())),
            }}
          >
            {
              questList.map((quest, questIndex) => (
                <div
                  className={clsx(classes['quest-videos__item'], quest.isAnounce && quest.startMonth && classes.disabled)}
                  onClick={memoOnQuestClick(quest.url, quest.isAnounce)}
                  key={quest._id}
                >
                  <div>
                    {
                      quest.isAnounce && (
                        <div
                          className={classes.flagAnounce}
                        >
                          <img src={FlagImg} alt="" />
                          <span>
                            старт
                            <br />
                            {quest.startMonth?.trim()}
                          </span>
                        </div>
                      )
                    }
                    <picture>
                      <source srcSet={getStaticPath(quest.photo)} type="image/webp" />
                      <img src={getStaticPath(quest.fallbackPhoto)} alt="" />
                    </picture>
                  </div>
                </div>
              ))
            }
          </SwipeSlider>
        </div>
        <Arrow onClick={handleOnArrowClick(true)} isNext />
      </div>
    );
  }
};

export default Home;
