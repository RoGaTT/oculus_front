import React, { FC } from 'react';
import { Helmet } from 'react-helmet-async';
import MainPersonImg from '@/assets/img/main_person.svg';
import Home from './Home';

const HomePage: FC = () => (
  <>
    <Helmet title="Oculus University - Платформа обучения">
      <link rel="preload" as="image" href={MainPersonImg} />
    </Helmet>
    <Home />
  </>
);

export default HomePage;
