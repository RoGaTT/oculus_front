import React, { FC } from 'react';

import { CourseAuthor } from '@/types/course';

import CrossIcon from '@/assets/img/cross.svg';
import SettingsIcon from '@/assets/img/settings.svg';
import classes from './AuthorCard.module.scss';

type PropsType = {
  author: CourseAuthor
  onDelete: { (): void }
  onEdit?: { (): void }
}

const AuthorCard: FC<PropsType> = ({
  author,
  onDelete,
  onEdit,
}) => (
  <div className={classes.root}>
    <div className={classes.head}>
      <h6>{author.name}</h6>
      <div>
        <button type="button" onClick={onEdit}>
          <img src={SettingsIcon} alt="" />
        </button>
        <button type="button" onClick={onDelete}>
          <img src={CrossIcon} alt="" />
        </button>
      </div>
    </div>
  </div>
);

export default AuthorCard;
