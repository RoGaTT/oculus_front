import React, {
  ChangeEvent,
  FC, FormEvent, useCallback, useContext, useEffect, useState,
} from 'react';

import clsx from 'clsx';
import Button from '@/ui/Button';
import { ILectureCreate } from '@/api/lecture';
import APIContext from '@/context/api';
import { ModalSettingModeEnum } from '@/const/modal_config';
import { LectureModel } from '@/types/models';
import ModalContext, { ModalTypeEnum } from '@/context/modal';
import classes from './LectureModal.module.scss';

export type LectureModalData = {
  id?: string
  mode?: ModalSettingModeEnum,
  onDelete?: { (): Promise<void> }
}

const LectureModal: FC<LectureModalData> = ({
  id,
  mode = ModalSettingModeEnum.CREATE,
  onDelete,
}) => {
  const IMAGE_INPUT_NAME = 'lecture_photo';

  const [data, setData] = useState<ILectureCreate>({
    text: '',
    video: '',
    photo: undefined,
  });

  const api = useContext(APIContext);
  const { closeModal, openModal } = useContext(ModalContext);

  const handleRefetch = useCallback(refetch, [api.lecture, id]);
  const handleOnInputChange = useCallback(onInputChange, [data]);
  const handleOnFileChange = useCallback(onFileChange, [onFileChange]);
  const handleOnSubmit = useCallback(onSubmit, [api.lecture, closeModal, data, handleRefetch, id, mode, openModal]);
  const memoOnDelete = useCallback(handleOnDelete, [api.lecture, closeModal, handleRefetch, id, onDelete]);

  useEffect(() => {
    if (id) handleRefetch();
  }, [api.lecture, handleRefetch, id]);

  return (
    <form className={clsx(classes.root)} onSubmit={handleOnSubmit}>
      <h5>Видеолекция</h5>
      <label>
        <div>
          <span>
            Название лекции
            {' '}
            <span>*</span>
          </span>
          <span>
            {data.text?.length || 0}
            /184
          </span>
        </div>
        <textarea
          rows={5}
          maxLength={184}
          value={data.text}
          placeholder="Введите текст"
          onChange={handleOnInputChange('text')}
        />
      </label>
      <label>
        <span>
          Введите ссылку на видео лекции
          {' '}
          <span>*</span>
        </span>
        <input
          type="text"
          placeholder="Введите ссылку"
          value={data.video}
          onChange={handleOnInputChange('video')}
        />
      </label>
      <label htmlFor={IMAGE_INPUT_NAME}>
        <span>
          Превью видеолекции
          {' '}
          <span>*</span>
          {' '}
          (.jpeg, .jpg, .png, .gif)
        </span>
        <div className={classes.fileLabel}>
          <input type="file" accept=".jpeg,.jpg,.png,.gif" name={IMAGE_INPUT_NAME} id={IMAGE_INPUT_NAME} onChange={handleOnFileChange} hidden />
          <span>Выбрать файл</span>
          <p>{data.photo?.name || 'Не выбрано'}</p>
        </div>
      </label>
      <Button type="submit">Сохранить</Button>
      {
        id && <Button className={classes.deleteButton} onClick={memoOnDelete}>Удалить лекцию</Button>
      }
    </form>
  );

  function refetch() {
    if (!id) return;
    api.lecture.getById(id).then(((data) => {
      setData({
        text: (data as LectureModel)?.text,
        video: (data as LectureModel)?.video,
      });
    }));
  }

  function onInputChange(key: keyof ILectureCreate) {
    return (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>) => {
      setData({
        ...data,
        [key]: e.target.value,
      });
    };
  }

  async function handleOnDelete() {
    if (!id) return;
    await api.lecture.deleteById(id);
    closeModal(ModalTypeEnum.COURSE_LECTURE_EDIT)();
    await onDelete?.();
    await handleRefetch();
  }

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (mode === ModalSettingModeEnum.CREATE) {
      await api.lecture.create(data);
      closeModal(ModalTypeEnum.COURSE_LECTURE_CREATE)();
    }
    if (mode === ModalSettingModeEnum.EDIT && id) {
      await api.lecture.update(id, data);
      closeModal(ModalTypeEnum.COURSE_LECTURE_EDIT)();
    }
    await handleRefetch();
    openModal(ModalTypeEnum.STATUS)();
  }

  async function onFileChange(e: ChangeEvent<HTMLInputElement>) {
    const file = e.target.files?.[0];

    setData({
      ...data,
      photo: file,
    });
  }
};

export default LectureModal;
