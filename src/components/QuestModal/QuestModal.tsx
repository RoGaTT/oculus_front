import React, {
  ChangeEvent,
  FC, FormEvent, useCallback, useContext, useEffect, useState,
} from 'react';

import clsx from 'clsx';
import _ from 'lodash';
import Button from '@/ui/Button';
import { IQuestCreate } from '@/api/quest';
import APIContext from '@/context/api';
import { ModalSettingModeEnum } from '@/const/modal_config';
import { QuestModel } from '@/types/models';
import ModalContext, { ModalTypeEnum } from '@/context/modal';
import { CourseMonthEnum } from '@/types/course';
import classes from './QuestModal.module.scss';

export type QuestModalData = {
  id?: string
  mode?: ModalSettingModeEnum,
}

const QuestModal: FC<QuestModalData> = ({
  id,
  mode = ModalSettingModeEnum.CREATE,
}) => {
  const IMAGE_INPUT_NAME = 'quest_photo';

  const [data, setData] = useState<IQuestCreate>({
    title: '',
    description: '',
    startMonth: undefined,
    isAnounce: true,
    photo: undefined,
    url: '',
  });

  const [isMain, setMainState] = useState<boolean>(false);

  const api = useContext(APIContext);
  const { closeModal } = useContext(ModalContext);

  const handleRefetch = useCallback(refetch, [api.quest, id]);
  const handleOnInputChange = useCallback(onInputChange, [data]);
  const handleOnFileChange = useCallback(onFileChange, [onFileChange]);
  const handleOnDelete = useCallback(onDelete, [api.quest, closeModal, handleRefetch, id]);
  const handleOnSubmit = useCallback(onSubmit, [
    api.metadata, api.quest, closeModal, data, handleRefetch, id, isMain, mode,
  ]);

  useEffect(() => {
    if (id) handleRefetch();
  }, [api.quest, handleRefetch, id]);

  useEffect(() => {
    if (id) {
      api.metadata.getMainQuest().then((data) => {
        setMainState(data._id === id);
      });
    }
  }, [api.metadata, id]);

  return (
    <form className={clsx(classes.root)} onSubmit={handleOnSubmit}>
      <h5>Квест</h5>
      <label>
        <span>
          Название квеста
          {' '}
          <span>*</span>
        </span>
        <input
          type="text"
          placeholder="Введите название"
          value={data.title}
          onChange={handleOnInputChange('title')}
        />
      </label>
      <label>
        <div>
          <span>
            Описание квеста
            {' '}
            <span>*</span>
          </span>
          <span>
            {data.description?.length || 0}
            /184
          </span>
        </div>
        <textarea
          rows={5}
          maxLength={184}
          value={data.description}
          placeholder="Введите текст"
          onChange={handleOnInputChange('description')}
        />
      </label>
      <label>
        <span>
          ID квеста
          {' '}
          <span>*</span>
        </span>
        <input
          type="text"
          placeholder="Введите ID (например 2)"
          value={data.url}
          onChange={handleOnInputChange('url')}
        />
      </label>
      <label htmlFor={IMAGE_INPUT_NAME}>
        <span>
          Превью квеста
          {' '}
          <span>*</span>
          {' '}
          (.jpeg, .jpg, .png, .gif)
        </span>
        <div className={classes.fileLabel}>
          <input type="file" accept=".jpeg,.jpg,.png,.gif" name={IMAGE_INPUT_NAME} id={IMAGE_INPUT_NAME} onChange={handleOnFileChange} hidden />
          <span>Выбрать файл</span>
          <p>{data.photo?.name || 'Не выбрано'}</p>
        </div>
      </label>
      <h5>Сделать анонс</h5>
      <div>
        <label className={clsx(data.isAnounce && classes.isChecked)}>
          <input
            type="checkbox"
            onChange={onInputChange('isAnounce')}
            checked={data.isAnounce}
          />
        </label>
        <label htmlFor="startMonth">Анонс курса</label>
        <select
          id="startMonth"
          disabled={!data.isAnounce}
          onChange={handleOnInputChange('startMonth')}
          placeholder="Выберите месяц"
          value={data.startMonth}
        >
          {
            Object.entries(CourseMonthEnum).map(([key, value]) => (
              <option value={value} key={key}>{_.upperFirst(value)}</option>
            ))
          }
        </select>
      </div>
      <div className={classes.isMain}>
        <label className={clsx(isMain && classes.isChecked)}>
          <input
            type="checkbox"
            onChange={(e) => {
              setMainState(e.target.checked);
            }}
            checked={isMain}
          />
        </label>
        <label htmlFor="isMain">Сделать Главным</label>
      </div>
      <Button type="submit">Сохранить</Button>
      {
        id && <Button className={classes.deleteButton} onClick={handleOnDelete}>Удалить квест</Button>
      }
    </form>
  );

  function refetch() {
    if (!id) return;
    api.quest.getById(id).then(((data) => {
      const buffer = data as QuestModel;
      setData({
        isAnounce: buffer.isAnounce,
        startMonth: buffer?.startMonth,
        url: buffer.url,
        title: buffer.title,
        description: buffer.description,
      });
    }));
  }

  function onInputChange(key: keyof IQuestCreate) {
    return (
      e: React.ChangeEvent<HTMLTextAreaElement> |
          React.ChangeEvent<HTMLSelectElement> |
          React.ChangeEvent<HTMLInputElement>,
    ) => {
      if (key === 'isAnounce') {
        setData({
          ...data,
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          isAnounce: (e.target as any)?.checked,
        });
      } else if (key === 'url') {
        setData({
          ...data,
          [key]: e.target.value.replace(/\D/ig, ''),
        });
      } else {
        setData({
          ...data,
          [key]: e.target.value,
        });
      }
    };
  }

  async function onDelete() {
    if (!id) return;
    await api.quest.deleteById(id);
    closeModal(ModalTypeEnum.QUEST_EDIT)();
    await handleRefetch();
  }

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (mode === ModalSettingModeEnum.CREATE) {
      const res = (await api.quest.create(data) as QuestModel);
      if (isMain) await api.metadata.setMainQuest(res._id);
      closeModal(ModalTypeEnum.QUEST_CREATE)();
    }
    if (mode === ModalSettingModeEnum.EDIT && id) {
      await api.quest.update(id, data);
      if (isMain) await api.metadata.setMainQuest(id);
      closeModal(ModalTypeEnum.QUEST_EDIT)();
    }
    await handleRefetch();
  }

  async function onFileChange(e: ChangeEvent<HTMLInputElement>) {
    const file = e.target.files?.[0];

    setData({
      ...data,
      photo: file,
    });
  }
};

export default QuestModal;
