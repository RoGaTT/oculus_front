import React, {
  ChangeEvent,
  FC, FormEvent, useCallback, useContext, useEffect, useState,
} from 'react';

import clsx from 'clsx';
import Button from '@/ui/Button';
import { IAuthorCreate } from '@/api/author';
import APIContext from '@/context/api';
import { ModalSettingModeEnum } from '@/const/modal_config';
import { AuthorModel } from '@/types/models';
import ModalContext, { ModalTypeEnum } from '@/context/modal';
import classes from './AuthorModal.module.scss';

export type AuthorModalData = {
  id?: string
  mode?: ModalSettingModeEnum,
  onDelete?: { (): Promise<void> }
}

const AuthorModal: FC<AuthorModalData> = ({
  id,
  onDelete,
  mode = ModalSettingModeEnum.CREATE,
}) => {
  const IMAGE_INPUT_NAME = 'author_photo';

  const [data, setData] = useState<IAuthorCreate>({
    name: '',
    info: '',
    shortName: '',
    photo: undefined,
  });

  const api = useContext(APIContext);
  const { openModal, closeModal } = useContext(ModalContext);

  const handleRefetch = useCallback(refetch, [api.author, id]);
  const handleOnInputChange = useCallback(onInputChange, [data]);
  const handleOnFileChange = useCallback(onFileChange, [onFileChange]);
  const handleOnSubmit = useCallback(onSubmit, [api.author, data, handleRefetch, id, mode, openModal]);
  const memoOnDelete = useCallback(handleOnDelete, [api.author, closeModal, id, onDelete]);

  useEffect(() => {
    if (id) handleRefetch();
  }, [api.author, handleRefetch, id]);

  return (
    <form className={clsx(classes.root)} onSubmit={handleOnSubmit}>
      <h5>Лектор</h5>
      <label>
        <span>
          ФИО
          {' '}
          <span>*</span>
        </span>
        <input
          type="text"
          placeholder="Введите имя лектора"
          value={data.name}
          onChange={handleOnInputChange('name')}
        />
      </label>
      <label>
        <span>
          Короткое имя (для отображения в начале страницы)
          {' '}
          <span>*</span>
        </span>
        <input
          type="text"
          placeholder="Введите короткое имя лектора (И.И. Иваненко)"
          value={data.shortName}
          onChange={handleOnInputChange('shortName')}
        />
      </label>
      <label>
        <div>
          <span>
            Описание лектора
            {' '}
            <span>*</span>
          </span>
          <span>
            {data.info?.length || 0}
            /228
          </span>
        </div>
        <textarea
          rows={5}
          maxLength={228}
          value={data.info}
          placeholder="Введите описание лектора"
          onChange={handleOnInputChange('info')}
        />
      </label>
      <label htmlFor={IMAGE_INPUT_NAME}>
        <span>
          Аватарка лектора
          {' '}
          <span>*</span>
          {' '}
          (.jpeg, .jpg, .png, .gif)
        </span>
        <div className={classes.fileLabel}>
          <input type="file" accept=".jpeg,.jpg,.png,.gif" name={IMAGE_INPUT_NAME} id={IMAGE_INPUT_NAME} onChange={handleOnFileChange} hidden />
          <span>Выбрать файл</span>
          <p>{data.photo?.name || 'Не выбрано'}</p>
        </div>
      </label>
      <Button type="submit">Сохранить</Button>
      {
        id && <Button className={classes.deleteButton} onClick={memoOnDelete}>Удалить лектора</Button>
      }
    </form>
  );

  function refetch() {
    if (!id) return;
    api.author.getById(id).then(((data) => {
      setData({
        info: (data as AuthorModel)?.info,
        name: (data as AuthorModel)?.name,
        shortName: (data as AuthorModel)?.shortName,
      });
    }));
  }

  function onInputChange(key: keyof IAuthorCreate) {
    return (e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>) => {
      setData({
        ...data,
        [key]: e.target.value,
      });
    };
  }

  async function handleOnDelete() {
    if (!id) return;
    await api.author.deleteById(id);
    await onDelete?.();
    closeModal(ModalTypeEnum.COURSE_AUTHOR_EDIT)();
  }

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (mode === ModalSettingModeEnum.CREATE) await api.author.create(data);
    if (mode === ModalSettingModeEnum.EDIT && id) await api.author.update(id, data);
    await handleRefetch();
    openModal(ModalTypeEnum.STATUS)();
  }

  async function onFileChange(e: ChangeEvent<HTMLInputElement>) {
    const file = e.target.files?.[0];

    setData({
      ...data,
      photo: file,
    });
  }
};

export default AuthorModal;
