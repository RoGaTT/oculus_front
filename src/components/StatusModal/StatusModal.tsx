/* eslint-disable jsx-a11y/control-has-associated-label */
import React, {
  FC, useCallback, useContext,
} from 'react';

import { useHistory } from 'react-router-dom';
import SuccessIcon from '@/assets/img/success_icon.svg';
import ModalContext, { ModalTypeEnum } from '@/context/modal';
import classes from './StatusModal.module.scss';

export type StatusModalData = {

}

const VerifyModal: FC<StatusModalData> = () => {
  const history = useHistory();

  const { closeModal, closeAllModal } = useContext(ModalContext);

  const memoOnRedirect = useCallback(handleOnRedirect, [closeAllModal, history]);
  const memoOnContinue = useCallback(handleOnContinue, [closeModal]);

  return (
    <div className={classes.root}>
      <img src={SuccessIcon} alt="" />
      <h3>Изменения внесены успешно</h3>
      <div className="controls">
        <button type="button" onClick={memoOnRedirect()}>Вернуться на страницу</button>
        <button type="button" onClick={memoOnContinue()}>Продолжить редактирование</button>
      </div>
    </div>
  );

  function handleOnContinue() {
    return () => {
      closeModal(ModalTypeEnum.STATUS)();
    };
  }

  function handleOnRedirect() {
    return () => {
      closeAllModal()();
      history.push('/');
    };
  }
};

export default VerifyModal;
