/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/no-array-index-key */
import React, {
  FC, useCallback, useContext, useMemo, useState,
} from 'react';

import { useCookies } from 'react-cookie';
import Button from '@/ui/Button';
import ModalContext from '@/context/modal';
import { Test, TEST_LIST } from '@/const/tests';
import { CookiesMapEnum } from '@/types/config';
import classes from './InitTestModal.module.scss';

export type InitTestModalData = {
}

const InitTestModal: FC<InitTestModalData> = () => {
  const [isFailure, setFailureState] = useState<boolean>(false);

  const [, setCookies] = useCookies([
    CookiesMapEnum.IS_INIT_TEST_PASSED,
  ]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const memoTest: Test = useMemo(() => TEST_LIST[Math.floor(Math.random() * 3)], [isFailure]);

  const { closeAllModal } = useContext(ModalContext);

  const handleOnAnswerClick = useCallback(onAnswerClick, [closeAllModal, setCookies]);
  const handleOnRepeatTestClick = useCallback(onRepeatTestClick, []);

  return !isFailure ? (
    <div className={classes.root}>
      <div className={classes.header}>
        <h2>Сайт предназначен только для медицинских и фармацевтических работников.</h2>
      </div>
      <p>{memoTest.question}</p>
      <div className={classes.options}>
        {
          memoTest.options.map((option, optionIndex) => (
            <div
              key={optionIndex}
              onClick={handleOnAnswerClick(!!option.isRight)}
            >
              <span />
              <p>{option.text}</p>
            </div>
          ))
        }
      </div>
    </div>
  ) : (
    <div className={classes.failure}>
      <h3>Вы ответили неверно</h3>
      <p>
        Материалы сайта предназначены
        <br />
        только для медицинских и фармацевтических работников
      </p>
      <Button onClick={handleOnRepeatTestClick}>Пройти еще раз</Button>
    </div>
  );

  function onAnswerClick(isRight: boolean) {
    return () => {
      if (isRight) {
        closeAllModal()();
        setCookies(CookiesMapEnum.IS_INIT_TEST_PASSED, 'true');
      } else setFailureState(true);
    };
  }

  function onRepeatTestClick() {
    setFailureState(false);
  }
};

export default InitTestModal;
