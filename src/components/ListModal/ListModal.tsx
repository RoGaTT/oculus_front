/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, {
  FC, useCallback, useContext, useEffect, useMemo, useState,
} from 'react';

import clsx from 'clsx';

// import VisibleIcon from '@/assets/img/visible_icon.svg';
import HiddenIcon from '@/assets/img/hidden_icon.svg';
import VisibleIcon from '@/assets/img/visible_icon.svg';
import SettingsIcon from '@/assets/img/settings_icon.svg';
import DeleteIcon from '@/assets/img/delete_icon.svg';
import ArrowIcon from '@/assets/img/arrow_move.svg';
import Button from '@/ui/Button';
import ModalContext, { ModalTypeEnum } from '@/context/modal';
import { removeBrFromText } from '@/utils/functions/text';
import classes from './ListModal.module.scss';

type ListItem = {
  id: string,
  text: string,
  isActive?: boolean
}

export type ListModalData = {
  title: string,
  formatData: (list: any) => ListItem[],
  onRefetch: { (): any },
  onSetVisibleState?: { (id: string): unknown },
  onEdit?: { (id: string): unknown },
  onDelete?: { (id: string): unknown },
  onAdd: { (): unknown }
  onSave?: { (idList: string[]): unknown }
}

const ListModal: FC<ListModalData> = ({
  title,
  formatData,
  onRefetch,
  onEdit,
  onDelete,
  onAdd,
  onSave,
}) => {
  const [list, setList] = useState<ListItem[]>([]);

  const { openModal } = useContext(ModalContext);

  const refetch = useCallback(() => {
    onRefetch?.().then(formatData).then(setList);
  }, [formatData, onRefetch]);

  useEffect(() => {
    refetch();
  }, [formatData, onRefetch, refetch]);

  const memoList = useMemo(() => list.sort((first, second) => {
    if (first.isActive && second.isActive) return 0;
    if (first.isActive) return -1;
    return 1;
  }), [list]);

  const memoActiveItemsCount = useMemo(
    () => memoList.reduce((accum, el) => (el.isActive ? accum + 1 : accum), 0),
    [memoList],
  );

  const memoOnSetVisibleState = useCallback(handleOnSetVisibleState, [list]);
  const memoOnEdit = useCallback(handleOnEdit, [onEdit]);
  const memoOnDelete = useCallback(handleOnDelete, [onDelete, openModal, refetch]);
  const memoOnAdd = useCallback(handleOnAdd, [onAdd]);
  const memoOnMove = useCallback(handleOnMove, [memoList]);
  const memoOnSave = useCallback(handleOnSave, [memoList, onSave, openModal]);

  return (
    <div className={classes.root}>
      <h3>{title}</h3>
      <div className={classes.controls}>
        <Button type="button" onClick={memoOnSave()}>Сохранить</Button>
        <Button type="button" onClick={memoOnAdd()}>Добавить</Button>
      </div>
      {
        memoList.map((el, elIndex) => (
          <div key={el.id} className={clsx(el.isActive && classes.isActive)}>
            <p>{removeBrFromText(el.text)}</p>
            <div>
              <button type="button" onClick={memoOnSetVisibleState(el.id)}>
                <img src={el.isActive ? HiddenIcon : VisibleIcon} alt="" />
              </button>
              {
                onEdit && (
                  <button type="button" onClick={memoOnEdit(el.id)}>
                    <img src={SettingsIcon} alt="" />
                  </button>
                )
              }
              {
                onDelete && (
                  <button type="button" onClick={memoOnDelete(el.id)}>
                    <img src={DeleteIcon} alt="" />
                  </button>
                )
              }
              <div>
                {
                  el.isActive && (
                    <>
                      <button type="button" onClick={memoOnMove(el.id, 'up')} disabled={!elIndex}>
                        <img src={ArrowIcon} alt="" />
                      </button>
                      <button type="button" onClick={memoOnMove(el.id, 'down')} disabled={elIndex === memoActiveItemsCount - 1}>
                        <img src={ArrowIcon} alt="" />
                      </button>
                    </>
                  )
                }
              </div>
            </div>
          </div>
        ))
      }
    </div>
  );

  function handleOnSave() {
    return async () => {
      await onSave?.(memoList.filter((el) => el.isActive).map((el) => el.id));
      openModal(ModalTypeEnum.STATUS)();
    };
  }

  function handleOnMove(id: string, direction: 'down' | 'up') {
    return () => {
      const listCopy = [...memoList];
      const elIndex = listCopy.findIndex((el) => el.id === id);
      if (elIndex === -1) return;
      const deletedItem = listCopy.splice(elIndex, 1);
      listCopy.splice(elIndex + (direction === 'down' ? 1 : -1), 0, deletedItem[0]);
      setList(listCopy);
    };
  }

  function handleOnSetVisibleState(id: string) {
    return async () => {
      setList(list.map((el) => (el.id !== id ? el : {
        ...el,
        isActive: !el.isActive,
      })));
    };
  }

  function handleOnEdit(id: string) {
    return async () => {
      onEdit?.(id);
    };
  }

  function handleOnDelete(id: string) {
    return async () => {
      await onDelete?.(id);
      await refetch();
      openModal(ModalTypeEnum.STATUS)();
    };
  }
  function handleOnAdd() {
    return () => {
      onAdd?.();
    };
  }
};

export default ListModal;
