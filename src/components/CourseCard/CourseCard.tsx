/* eslint-disable react/no-danger */
/* eslint-disable react/no-array-index-key */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable max-len */
import React, {
  FC, useCallback, useMemo, useRef,
} from 'react';
import clsx from 'clsx';
import SwipeSlider from 'react-swipe';

import LineImg from '@/assets/img/library_line.webp';
import LineFallbackImg from '@/assets/img/fallback/library_line.png';
import FlagImg from '@/assets/img/library_flag.svg';
import ArrowOpenIcon from '@/assets/img/arrow_open.svg';
import ArrowSliderIcon from '@/assets/img/slider_arrow.svg';
import Button from '@/ui/Button';
import combineWordsWithNumber, { removeBrFromText } from '@/utils/functions/text';
import { CourseModel } from '@/types/models';
import { getStaticPath } from '@/utils/functions/path';
import classes from './CourseCard.module.scss';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface PropsType extends CourseModel {
  isOpened: boolean,
  onOpen: { (): void },
}

interface ArrowPropsType {
  onClick: { (): void },
  isNext?: boolean,
}

const Arrow: FC<ArrowPropsType> = ({
  isNext,
  onClick,
}) => (
  <div className={clsx(classes['content-videos__slider-arrow__wrapper'], isNext && classes.next)}>
    <button
      type="button"
      onClick={onClick}
      className={classes['content-videos__slider-arrow']}
    >
      <img src={ArrowSliderIcon} alt="" />
    </button>
  </div>
);

const CourseCard: FC<PropsType> = ({
  isOpened,
  onOpen,
  isAnounce,
  title,
  description,
  startMonth,
  lectures,
  authors,
}) => {
  const sliderRef = useRef<SwipeSlider | null>(null);
  // let sliderRef: SwipeSlider | undefined | null;
  const handleOnArrowClick = useCallback(onArrowClick, []);

  const memoAuthors = useMemo(() => authors.sort((a, b) => ((a.data.shortName?.trim() || a.data.name?.trim()) > (b.data.shortName?.trim() || b.data.name?.trim()) ? 1 : -1)), [authors]);

  return (
    <div className={classes.root}>
      {
        !isAnounce && (
          <div className={classes['openButton-wrapper']}>
            <button
              type="button"
              onClick={onOpen}
              className={clsx(classes.openButton, isOpened && classes.isOpened)}
            >
              <img src={ArrowOpenIcon} alt="" />
            </button>
          </div>
        )
      }
      <div>
        <div className={classes.card}>
          <div className={clsx(classes['card-text'], isAnounce && classes.disabled)}>
            <div>
              <h4>{removeBrFromText(title)}</h4>
            </div>
            <p>{description}</p>
          </div>
          <div className={clsx(classes['card-lectures'], isAnounce && classes.disabled)}>
            <div className={classes['card-lectures__text']}>
              <span>{lectures.length}</span>
              <p>{combineWordsWithNumber(lectures.length, 'лекци')}</p>
            </div>
            {
              isAnounce && startMonth && (
                <div className={clsx(classes['card-lectures__flag'], classes.disabled)}>
                  <img src={FlagImg} alt="" />
                  <span>
                    старт
                    <br />
                    {startMonth.trim()}
                  </span>
                </div>
              )
            }
            {
              !isAnounce && (
                <Button className={classes['card-lectures__openButton']} onClick={onOpen}>{isOpened ? 'Свернуть' : 'Развернуть'}</Button>
              )
            }
          </div>
          <picture>
            <source srcSet={LineImg} type="image/webp" className={clsx(isAnounce && classes.disabled)} />
            <img src={LineFallbackImg} alt="" className={clsx(isAnounce && classes.disabled)} />
          </picture>
        </div>
        {
          isOpened && (
            <div className={clsx(classes.content, 'animate__animated', 'animate__fadeInRight')}>
              <div className={classes['content-authors']}>
                <h4>Курс читают:</h4>
                {
                  memoAuthors.map((author, authorIndex) => (
                    <div key={`lecture__author__${authorIndex}`} className={classes['content-authors__item']}>
                      <div className={classes['content-authors__item-photo']}>
                        <picture>
                          <source srcSet={getStaticPath(author.data.photo)} type="image/webp" />
                          <img src={getStaticPath(author.data.fallbackPhoto)} alt={author.data.name} />
                        </picture>
                      </div>
                      <div className={classes['content-authors__item-text']}>
                        <span>{author.data.name}</span>
                        <p>{author.data.info}</p>
                      </div>
                    </div>
                  ))
                }
              </div>
              <div className={classes['content-videos']}>
                <Arrow onClick={handleOnArrowClick(false)} />
                <div className={classes['content-videos__slider-wrapper']}>
                  <SwipeSlider
                    className={classes['content-videos__slider']}
                    swipeOptions={{
                      speed: 500,
                      stopPropagation: true,
                    }}
                    childCount={5}
                    ref={(el) => { sliderRef.current = el; }}
                    // ref={(el) => { sliderRef = el; }}
                  >
                    {
                      lectures.map((lecture, lectureIndex) => (
                        <div key={lecture._id} className={classes['content-videos__item']}>
                          <h5>
                            Лекция
                            {' '}
                            {lectureIndex + 1}
                            {' '}
                            из
                            {' '}
                            {lectures.length}
                          </h5>
                          {
                            lecture.data.fallbackPhoto && !lecture.data.video && (
                              <img src={getStaticPath(lecture.data.fallbackPhoto)} alt="" />
                            )
                          }
                          {
                            lecture.data.video && (
                              <div id={`lecture_video_${lectureIndex}`} dangerouslySetInnerHTML={{ __html: lecture.data.video }} />
                            )
                          }
                          <p>{lecture.data.text}</p>
                        </div>
                      ))
                    }
                  </SwipeSlider>
                </div>
                <Arrow onClick={handleOnArrowClick(true)} isNext />
              </div>
            </div>
          )
        }
      </div>
    </div>
  );
  function onArrowClick(isNext = false): () => void {
    return () => {
      // const { current: slider } = sliderRef;
      // const slider = sliderRef;
      if (!sliderRef.current) return;
      if (isNext) sliderRef.current.next();
      else sliderRef.current.prev();
    };
  }
};

export default CourseCard;
