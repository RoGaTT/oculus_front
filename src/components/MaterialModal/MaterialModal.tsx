import React, {
  ChangeEvent,
  FC, FormEvent, useCallback, useContext, useEffect, useState,
} from 'react';

import clsx from 'clsx';
import Button from '@/ui/Button';
import { IMaterialCreate } from '@/api/material';
import APIContext from '@/context/api';
import { ModalSettingModeEnum } from '@/const/modal_config';
import { MaterialModel } from '@/types/models';
import ModalContext, { ModalTypeEnum } from '@/context/modal';
import classes from './MaterialModal.module.scss';

export type MaterialModalData = {
  id?: string
  mode?: ModalSettingModeEnum,
}

const MaterialModal: FC<MaterialModalData> = ({
  id,
  mode = ModalSettingModeEnum.CREATE,
}) => {
  const IMAGE_INPUT_NAME = 'material_photo';

  const [data, setData] = useState<IMaterialCreate>({
    title: '',
    file: undefined,
    needVerify: false,
    url: '',
  });

  const api = useContext(APIContext);
  const { closeModal } = useContext(ModalContext);

  const handleRefetch = useCallback(refetch, [api.material, id]);
  const handleOnInputChange = useCallback(onInputChange, [data]);
  const handleOnFileChange = useCallback(onFileChange, [onFileChange]);
  const handleOnSubmit = useCallback(onSubmit, [api.material, closeModal, data, handleRefetch, id, mode]);
  const handleOnDelete = useCallback(onDelete, [api.material, closeModal, handleRefetch, id]);

  useEffect(() => {
    if (id) handleRefetch();
  }, [api.material, handleRefetch, id]);

  return (
    <form className={clsx(classes.root)} onSubmit={handleOnSubmit}>
      <h5>Материал</h5>
      <label>
        <div>
          <span>
            Название материала
            {' '}
            <span>*</span>
          </span>
          <span>
            {data.title?.length || 0}
            /184
          </span>
        </div>
        <textarea
          rows={5}
          value={data.title}
          placeholder="Введите текст"
          onChange={handleOnInputChange('title')}
        />
      </label>
      <label>
        <span>
          Ссылка на материал (очистите поле если хотите использовать файл)
          {' '}
          <span>*</span>
        </span>
        <input
          type="text"
          placeholder="Введите ссылку"
          value={data.url}
          onChange={handleOnInputChange('url')}
        />
      </label>
      <div className={classes.needVerify}>
        <label className={clsx(data.needVerify && classes.isChecked)}>
          <input
            type="checkbox"
            onChange={handleOnInputChange('needVerify')}
            checked={data.needVerify}
          />
        </label>
        <label htmlFor="needVerify">Предупредить о переходе на внешний сайт</label>
      </div>
      <label htmlFor={IMAGE_INPUT_NAME}>
        <span>
          Файл материала
          {' '}
          <span>*</span>
          {' '}
          (.*)
        </span>
        <div className={classes.fileLabel}>
          <input type="file" name={IMAGE_INPUT_NAME} id={IMAGE_INPUT_NAME} onChange={handleOnFileChange} hidden />
          <span>Выбрать файл</span>
          <p>{data.file?.name || 'Не выбрано'}</p>
        </div>
      </label>
      <Button type="submit">Сохранить</Button>
      {
        id && <Button className={classes.deleteButton} onClick={handleOnDelete}>Удалить материал</Button>
      }
    </form>
  );

  function refetch() {
    if (!id) return;
    api.material.getById(id).then(((data) => {
      setData({
        // ...data,
        title: (data as MaterialModel)?.title,
        needVerify: (data as MaterialModel)?.needVerify,
        url: (data as MaterialModel)?.url || '',
      });
    }));
  }

  function onInputChange(key: keyof IMaterialCreate) {
    return (
      e: React.ChangeEvent<HTMLTextAreaElement> |
          React.ChangeEvent<HTMLSelectElement> |
          React.ChangeEvent<HTMLInputElement>,
    ) => {
      if (key === 'needVerify') {
        setData({
          ...data,
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          needVerify: (e.target as any)?.checked,
        });
      } else {
        setData({
          ...data,
          [key]: e.target.value,
        });
      }
    };
  }

  async function onDelete() {
    if (!id) return;
    await api.material.deleteById(id);
    closeModal(ModalTypeEnum.MATERIAL_EDIT)();
    await handleRefetch();
  }

  async function onSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (mode === ModalSettingModeEnum.CREATE) {
      await api.material.create(data);
      closeModal(ModalTypeEnum.MATERIAL_CREATE)();
    }
    if (mode === ModalSettingModeEnum.EDIT && id) {
      await api.material.update(id, data);
      closeModal(ModalTypeEnum.MATERIAL_EDIT)();
    }
    await handleRefetch();
  }

  async function onFileChange(e: ChangeEvent<HTMLInputElement>) {
    const file = e.target.files?.[0];

    setData({
      ...data,
      file,
    });
  }
};

export default MaterialModal;
