import React, { FC } from 'react';

import { CourseLecture } from '@/types/course';

import CrossIcon from '@/assets/img/cross.svg';
import SettingsIcon from '@/assets/img/settings.svg';
import ArrowIcon from '@/assets/img/arrow_move.svg';
import classes from './LectureCard.module.scss';

type PropsType = {
  lecture: CourseLecture
  index: number
  onEdit: { (): void }
  onDelete: { (): void }
  onMove: { (direction: 'down' | 'up'): () => void }
  isFirst: boolean
  isLast: boolean
}

const LectureCard: FC<PropsType> = ({
  lecture,
  index,
  onEdit,
  onDelete,
  onMove,
  isFirst,
  isLast,
}) => (
  <div className={classes.root}>
    <div className={classes.head}>
      <h5>
        #
        {index + 1}
        {' '}
        Лекция
      </h5>
    </div>
    <div className={classes.content}>
      <p>{lecture.text}</p>
      <div>
        <button type="button" onClick={onEdit}>
          <img src={SettingsIcon} alt="" />
        </button>
        <button type="button" onClick={onDelete}>
          <img src={CrossIcon} alt="" />
        </button>
        <div>
          <button type="button" onClick={onMove('up')} disabled={isFirst}>
            <img src={ArrowIcon} alt="" />
          </button>
          <button type="button" onClick={onMove('down')} disabled={isLast}>
            <img src={ArrowIcon} alt="" />
          </button>
        </div>
      </div>
    </div>
  </div>
);

export default LectureCard;
