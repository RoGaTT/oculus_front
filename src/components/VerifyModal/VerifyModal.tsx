/* eslint-disable jsx-a11y/control-has-associated-label */
import React, {
  FC, useCallback,
} from 'react';

import classes from './VerifyModal.module.scss';

export type VerifyModalData = {
  title: string
  onAgree?: { (): void },
  onDecline?: { (): void }
}

const VerifyModal: FC<VerifyModalData> = ({
  onAgree,
  onDecline,
  title,
}) => {
  const memoOnAgree = useCallback(handleOnAgree, [onAgree]);
  const memoOnDecline = useCallback(handleOnDecline, [onDecline]);

  return (
    <div className={classes.root}>
      <h3>{title}</h3>
      <div className="controls">
        {
          onDecline && <button type="button" onClick={memoOnDecline()}>Отклонить</button>
        }
        {
          onAgree && <button type="button" onClick={memoOnAgree()}>Принять</button>
        }
      </div>
    </div>
  );

  function handleOnDecline() {
    return async () => {
      await onDecline?.();
    };
  }
  function handleOnAgree() {
    return () => {
      onAgree?.();
    };
  }
};

export default VerifyModal;
