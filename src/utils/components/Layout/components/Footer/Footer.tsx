/* eslint-disable max-len */
import React, { FC, useCallback, useMemo } from 'react';

import { useHistory } from 'react-router';
import PersonImg from '@/assets/img/news_person.webp';
import PersonFallbackImg from '@/assets/img/fallback/news_person.png';
import LogoImg from '@/assets/img/logo.svg';
import MessageIcon from '@/assets/img/message.svg';
import PhoneIcon from '@/assets/img/phone.svg';

import { goBlank } from '@/utils/functions/dom';

import Button from '@/ui/Button';
import Container from '@/utils/components/Container';

import ROUTES from '@/const/routes';
import classes from './Footer.module.scss';

type IProps = {
  noSubscribe?: boolean
}

const Footer: FC<IProps> = ({
  noSubscribe,
}) => {
  const history = useHistory();
  const memoActiveYear = useMemo(() => (new Date()).getFullYear(), []);

  const handleOnGo = useCallback(onGo, [history]);

  return (
    <Container className={classes.root}>
      {
        !noSubscribe && (
          <div className={classes.news}>
            <div className={classes['news-image']}>
              <picture>
                <source srcSet={PersonImg} type="image/webp" />
                <img src={PersonFallbackImg} alt="" />
              </picture>
            </div>
            <div className={classes['news-text']}>
              <h5>Хотите первым увидеть новые материалы?</h5>
              <div className={classes['news-text__image']}>
                <picture>
                  <source srcSet={PersonImg} type="image/webp" />
                  <img src={PersonFallbackImg} alt="" />
                </picture>
              </div>
              <p>Подпишитесь и узнавайте первым о появлении новых курсов и интерактивных квестов</p>
              <Button className={classes['news-text__button']} onClick={goBlank('http://nvssoglasie.ru/8z3rofccqie4')}>Подписаться</Button>
            </div>
          </div>
        )
      }
      <div className={classes.info}>
        <div className={classes['info-logo']}>
          <img src={LogoImg} alt="" />
          <div className={classes['info-logo__contact']}>
            <div>
              <img src={MessageIcon} alt="" />
            </div>
            <span>drug.safety_russia@novartis.com</span>
          </div>
          <div className={classes['info-logo__contact']}>
            <div>
              <img src={PhoneIcon} alt="" />
            </div>
            <span>+7 495 967 12 70</span>
          </div>
        </div>
        <div className={classes['info-text']}>
          <h6>Проект создан при поддержке компании Новартис совместно с ведущими врачами-офтальмологами.</h6>
          <p>Если у вас или вашего пациента возникло нежелательное явление на фоне применения препаратов группы компаний «Новартис» («Новартис Фарма», «Новартис Онкология» и «Сандоз»), пожалуйста, сообщите об этом в Единый центр по фармаконадзору</p>
          <div className={classes['info-text__contact']}>
            <div>
              <img src={MessageIcon} alt="" />
            </div>
            <span>drug.safety_russia@novartis.com</span>
          </div>
          <div className={classes['info-text__contact']}>
            <div>
              <img src={PhoneIcon} alt="" />
            </div>
            <span>+7 495 967 12 70</span>
          </div>
          <div className={classes['info-text__copyright']}>
            ©
            {memoActiveYear}
            &nbsp;&nbsp;
            <Button onClick={handleOnGo(ROUTES.PRIVACY_POLICY)}>Политика конфиденциальности</Button>
          </div>
        </div>
      </div>
    </Container>
  );

  function onGo(path: string) {
    return () => {
      history.push(path);
    };
  }
};

export default Footer;
