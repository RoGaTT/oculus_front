import React, {
  FC, useCallback, useState,
} from 'react';

import { useHistory } from 'react-router';
import Container from '@/utils/components/Container';
import LogoImg from '@/assets/img/logo.svg';
import BurgerImg from '@/assets/img/burger.svg';

import Button from '@/ui/Button';
import { goBlank, goWithScroll } from '@/utils/functions/dom';
import classes from './Header.module.scss';
import AsideMenu from '../AsideMenu';

const Header: FC = () => {
  const [isAsideOpened, setAsideOpenState] = useState<boolean>(false);

  const handleOnBurgerClick = useCallback(onBurgerClick, [isAsideOpened]);
  const history = useHistory();

  return (
    <div className={classes.wrapper}>
      <Container
        className={classes.root}
      >
        <a href="/">
          <img src={LogoImg} alt="" className={classes.logo} />
        </a>
        <div className={classes.navWrapper}>
          <nav>
            <button type="button" onClick={goWithScroll('/', 'courses', history)}>Курсы</button>
            <button type="button" onClick={goWithScroll('/', 'quests', history)}>Квесты</button>
            <button type="button" onClick={goWithScroll('/', 'materials', history)}>Полезные материалы</button>
          </nav>
        </div>
        <button
          type="button"
          onClick={onBurgerClick}
          className={classes.burger}
        >
          <img
            src={BurgerImg}
            alt=""
          />
        </button>
        <Button className={classes.button} onClick={goBlank('https://oculus-med.ru')}>На главную</Button>
      </Container>
      {
        isAsideOpened && (
          <AsideMenu onClose={handleOnBurgerClick} />
        )
      }
    </div>
  );
  function onBurgerClick() {
    setAsideOpenState(!isAsideOpened);
  }
};

export default Header;
