/* eslint-disable react/no-array-index-key */
import { DialogContent, Dialog } from '@material-ui/core';
import clsx from 'clsx';
import React, {
  FC, useCallback, useEffect, useMemo, useState,
} from 'react';
import { useCookies } from 'react-cookie';
import { useHistory } from 'react-router';
import APIAuth from '@/api/auth';
import APIAuthor from '@/api/author';
import APICourse from '@/api/course';
import APILecture from '@/api/lecture';
import APIMaterial from '@/api/material';
import APIMetadata from '@/api/metadata';
import APIQuest from '@/api/quest';
import MODAL_CONFIG from '@/const/modal_config';
import APIContext from '@/context/api';
import ModalContext, { ModalType, ModalTypeEnum, ModalTypeOptions } from '@/context/modal';
import { CookiesMapEnum } from '@/types/config';
import API from '@/utils/functions/api';

import initTestModalClasses from '@/components/InitTestModal/InitTestModal.module.scss';
import CookieAgreement from '../CookieAgreement';
import Header from './components/Header';

import classes from './Layout.module.scss';

type PropsType = {
    children: React.ReactNode
}

const Layout: FC<PropsType> = ({ children }) => {
  const [cookies] = useCookies([
    CookiesMapEnum.IS_COOKIES_AGREEMENT_ACCEPTED,
    CookiesMapEnum.BEARER_AUTH_TOKEN,
    CookiesMapEnum.IS_INIT_TEST_PASSED,
  ]);

  const history = useHistory();

  const [modalList, setModalList] = useState<ModalType[]>([]);

  const handleOpenModal = useCallback(openModal, [modalList]);
  const handleCloseModal = useCallback(closeModal, [modalList]);
  const handleCloseAllModal = useCallback(closeAllModal, []);

  const memoApi = useMemo(
    () => new API(cookies[CookiesMapEnum.BEARER_AUTH_TOKEN], history),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [cookies[CookiesMapEnum.BEARER_AUTH_TOKEN]],
  );

  const memoAuthApi = useMemo(() => new APIAuth(memoApi), [memoApi]);
  const memoAuthorApi = useMemo(() => new APIAuthor(memoApi), [memoApi]);
  const memoLectureApi = useMemo(() => new APILecture(memoApi), [memoApi]);
  const memoCourseApi = useMemo(() => new APICourse(memoApi), [memoApi]);
  const memoQuestApi = useMemo(() => new APIQuest(memoApi), [memoApi]);
  const memoMaterialApi = useMemo(() => new APIMaterial(memoApi), [memoApi]);
  const memoMetadataApi = useMemo(() => new APIMetadata(memoApi), [memoApi]);

  useEffect(() => {
    if (cookies[CookiesMapEnum.IS_INIT_TEST_PASSED] !== 'true') {
      handleOpenModal(ModalTypeEnum.INIT_TEST, {})({
        isClosingDisabled: true,
        classes: {
          paper: initTestModalClasses.wrapper,
          backdropRoot: initTestModalClasses.backdrop,
        },
        maxWidth: 'md',
      });
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cookies[CookiesMapEnum.IS_INIT_TEST_PASSED]]);

  return (
    <APIContext.Provider value={{
      base: memoApi,
      author: memoAuthorApi,
      lecture: memoLectureApi,
      auth: memoAuthApi,
      course: memoCourseApi,
      quest: memoQuestApi,
      material: memoMaterialApi,
      metadata: memoMetadataApi,
    }}
    >
      <ModalContext.Provider value={{
        modalList,
        openModal: handleOpenModal,
        closeModal: handleCloseModal,
        closeAllModal: handleCloseAllModal,
      }}
      >
        <div className={classes.root}>
          <Header />
          <div className={classes.content}>
            {children}
          </div>
          {
            !(cookies[CookiesMapEnum.IS_COOKIES_AGREEMENT_ACCEPTED] === 'true') && <CookieAgreement />
          }
          {
            modalList.map(({
              type, onClose, data, isClosingDisabled, classes: extraClasess, maxWidth,
            }) => (
              <Dialog
                key={type}
                open
                onClose={handleCloseModal(type, onClose)}
                disableEscapeKeyDown={isClosingDisabled}
                disableBackdropClick={isClosingDisabled}
                {
                  ...(maxWidth ? { maxWidth: 'md' } : {})
                }
                BackdropProps={{
                  classes: {
                    root: clsx(extraClasess?.backdropRoot),
                  },
                }}
                classes={{
                  paper: clsx(extraClasess?.paper),
                }}
              >
                <DialogContent className={clsx(classes['modal-content'])}>
                  <>
                    {
                      MODAL_CONFIG[type](data)
                    }
                  </>
                </DialogContent>
              </Dialog>
            ))
          }
        </div>
      </ModalContext.Provider>
    </APIContext.Provider>
  );
  function openModal(type: ModalTypeEnum, data: unknown, onClose?: { (): void }) {
    return (options: ModalTypeOptions = {}) => {
      setModalList([
        ...modalList,
        {
          type,
          data,
          onClose,
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          ...((options as unknown as any).target ? {} : options),
        },
      ]);
    };
  }
  function closeModal(type: ModalTypeEnum, onClose?: { (): void }) {
    return () => {
      onClose?.();
      setModalList(modalList.filter((el) => el.type !== type));
    };
  }
  function closeAllModal() {
    return () => {
      setModalList([]);
    };
  }
};

export default Layout;
