/* eslint-disable max-len */
import React, { FC, useCallback } from 'react';

import { useCookies } from 'react-cookie';
import { useHistory } from 'react-router';
import Button from '@/ui/Button';
import { CookiesMapEnum } from '@/types/config';
import ROUTES from '@/const/routes';
import classes from './CookieAgreement.module.scss';
import Container from '../Container';

const CookieAgreement: FC = () => {
  const [, setCookie] = useCookies([CookiesMapEnum.IS_COOKIES_AGREEMENT_ACCEPTED]);
  const history = useHistory();

  const handleOnAcceptClick = useCallback(onAcceptClick, [setCookie]);
  const handleOnGo = useCallback(onGo, [history]);

  return (
    <Container
      className={classes.root}
      wrapperClassName={classes['root-wrapper']}
    >
      <p>
        Oculus-university.ru использует cookies (файлы с данными о прошлых посещениях сайта) и сервисы сбора технических данных посетителей (данные о действиях на сайте, ip-адресе, местоположении и др.) Это позволяет нам анализировать взаимодействие посетителей с сайтом и делать его удобнее. Продолжая пользоваться сайтом, вы соглашаетесь
        {' '}
        <Button onClick={handleOnGo(ROUTES.PRIVACY_POLICY)}>с условиями использования сайта</Button>
        .
        Вы можете запретить сохранение cookies в настройках своего браузера.
      </p>
      <Button onClick={handleOnAcceptClick}>Принять правила</Button>
    </Container>
  );
  function onAcceptClick() {
    setCookie(CookiesMapEnum.IS_COOKIES_AGREEMENT_ACCEPTED, true);
  }
  function onGo(path: string) {
    return () => {
      history.push(path);
    };
  }
};

export default CookieAgreement;
