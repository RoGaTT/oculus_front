import wordEndingDict from '@/const/word_ending_dict';

const combineWordsWithNumber = (number: number, text: string): string => {
  const wordsInText = text.split(' ');
  let endNumber = 0;
  if (
    ((number % 100 >= 5) && (number % 100 <= 20)) || (((number % 10 >= 5) && (number % 10 <= 9)) || (number % 10 === 0))
  ) endNumber = 2;
  else if (
    (number % 10 >= 2) && (number % 10 <= 4) && (number % 100 !== 12) && (number % 100 !== 13) && (number % 100 !== 14)
  ) endNumber = 1;
  else if ((number % 100 !== 11) && (number % 10 === 1)) endNumber = 0;
  return `${wordsInText.map((word, index) => word + wordEndingDict[text][endNumber][index]).join(' ')}`;
};

export const removeBrFromText = (text: string): string => text.replace(/<[br\s/]{3,4}>/, '');

export default combineWordsWithNumber;
