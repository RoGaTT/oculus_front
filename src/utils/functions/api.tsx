/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-classes-per-file */
import axios, { AxiosError, AxiosResponse } from 'axios';
import { useHistory } from 'react-router-dom';
import ROUTES from '@/const/routes';
import { HeadersMapEnum } from '@/types/config';

type APIHeadersConfig = {
  [key in HeadersMapEnum]?: string
}

type APIMethodOptions<P> = {
  onSuccess?: { <K>(data: AxiosResponse<P>): K },
  onError?: { (error: AxiosError): void },
  onErrorRedirect?: boolean,
  fromRoot?: boolean
  headers?: {
    [key: string]: string
  },
}

class API {
  constructor(
    private readonly token: string | null,
    private readonly history: ReturnType<typeof useHistory>,
  ) {}

  isToken(): boolean {
    return !!this?.token;
  }

  private getHeaders(): APIHeadersConfig {
    if (!this.token) return {};
    return {
      [HeadersMapEnum.BEARER_AUTH_TOKEN]: this.token,
    };
  }

  // eslint-disable-next-line class-methods-use-this
  private formatResponse<P>(res: AxiosResponse<P>): P {
    return res.data;
  }

  async get<T, P>(
    path: string,
    data?: T,
    options?: APIMethodOptions<P>,
  ): Promise<AxiosResponse<P> | P | undefined > {
    let res;
    try {
      res = await axios({
        method: 'get',
        headers: {
          ...this.getHeaders(),
          ...options?.headers,
        },
        url: !options?.fromRoot ? `${process.env.REACT_APP_BASE}/api${path}` : `${process.env.REACT_APP_BASE}/${path}`,
        data,
      })
        .then((res: AxiosResponse<P>) => options?.onSuccess?.(res) || this.formatResponse(res));
    } catch (e: any) {
      if (options?.onErrorRedirect) this.history.replace(ROUTES.SIGN_IN);
      options?.onError?.(e);
      throw e;
    }

    return res;
  }

  async post<T, P>(
    path: string,
    data?: T,
    options?: APIMethodOptions<P>,
  ): Promise<AxiosResponse<P> | P | undefined > {
    let res;
    try {
      res = await axios({
        method: 'post',
        headers: {
          ...this.getHeaders(),
          ...options?.headers,
        },
        url: `${process.env.REACT_APP_BASE}/api${path}`,
        data,
      })
        .then((res: AxiosResponse<P>) => options?.onSuccess?.(res) || this.formatResponse(res));
    } catch (e: any) {
      if (options?.onErrorRedirect) this.history.replace(ROUTES.SIGN_IN);
      options?.onError?.(e);
      throw e;
    }

    return res;
  }

  async delete<T, P>(
    path: string,
    data?: T,
    options?: APIMethodOptions<P>,
  ): Promise<AxiosResponse<P> | P | undefined > {
    let res;
    try {
      res = await axios({
        method: 'delete',
        headers: {
          ...this.getHeaders(),
          ...options?.headers,
        },
        url: `${process.env.REACT_APP_BASE}/api${path}`,
        data,
      })
        .then((res: AxiosResponse<P>) => options?.onSuccess?.(res) || this.formatResponse(res));
    } catch (e: any) {
      if (options?.onErrorRedirect) this.history.replace(ROUTES.SIGN_IN);
      options?.onError?.(e);
      throw e;
    }

    return res;
  }

  async patch<T, P>(
    path: string,
    data?: T,
    options?: APIMethodOptions<P>,
  ): Promise<AxiosResponse<P> | P | undefined > {
    let res;
    try {
      res = await axios({
        method: 'patch',
        headers: {
          ...this.getHeaders(),
          ...options?.headers,
        },
        url: `${process.env.REACT_APP_BASE}/api${path}`,
        data,
      })
        .then((res: AxiosResponse<P>) => options?.onSuccess?.(res) || this.formatResponse(res));
    } catch (e: any) {
      if (options?.onErrorRedirect) this.history.replace(ROUTES.SIGN_IN);
      options?.onError?.(e);
      throw e;
    }

    return res;
  }
}

export default API;
