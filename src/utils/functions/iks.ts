// eslint-disable-next-line import/prefer-default-export
export const getIKSQuestLink = (userKey: string, onlyCase: string): string => `${process.env.REACT_APP_IKS_DOMAIN}/?app_key=${process.env.REACT_APP_IKS_APP_KEY}&user_key=${process.env.REACT_APP_IKS_USER_PREFIX}__${userKey}&only_case=${onlyCase}`;
