// eslint-disable-next-line import/prefer-default-export
export const getStaticPath = (path: string): string => `${process.env.REACT_APP_BASE}/static/files/${path}`;
