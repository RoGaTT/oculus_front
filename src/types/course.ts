export type CourseAuthor = {
  photo: string,
  fallbackPhoto: string,
  name: string,
  info: string,
}

export type CourseLecture = {
  fallbackImg?: string,
  img?: string,
  video?: string,
  text: string,
}

export enum CourseMonthEnum {
  JANUARY = 'в январе',
  FEBRUARY = 'в феврале',
  MARCH = 'в марте',
  APRIL = 'в апреле',
  MAY = 'в мае',
  JUNE = 'в июне',
  JULY = 'в июле',
  AUGUST = 'в августе',
  SEPTEMBER = 'в сентябре',
  OCTOBER = 'в октябре',
  NOVEMBER = 'в ноябре',
  DECEMBER = 'в декабре',
}

export type CourseData = {
  isActive: boolean,
  title: string,
  description: string,
  startMonth: CourseMonthEnum,
  authors: CourseAuthor[],
  lectures: CourseLecture[]
}

export type CourseDataInput = {
  isActive: CourseData['isActive'],
  title: CourseData['title'],
  description: CourseData['description'],
  startMonth: CourseData['startMonth'],
  authors?: CourseAuthor[],
  lectures?: CourseLecture[]
}

export type MaterialData = {
  title: string,
  url: string
}
