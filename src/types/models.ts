import { CourseMonthEnum } from './course';

type Photo = {
  photo: string,
  fallbackPhoto: string,
}

type Model = {
  _id: string
}

export type AuthorModel = Model & Photo & {
  name: string
  info: string
  shortName: string
}

export type LectureModel = Model & Photo & {
  text: string,
  video: string
}

export type CourseModel = Model & {
  title: string
  description: string
  isAnounce: boolean
  startMonth?: CourseMonthEnum
  lectures: Array<{
    _id: string
    data: LectureModel
  }>
  authors: Array<{
    _id: string
    data: AuthorModel
  }>
}

export type QuestModel = Model & Photo & {
  title: string
  description: string
  isAnounce: boolean
  startMonth?: CourseMonthEnum
  url: string
}

export type MaterialModel = Model & {
  title: string
  file?: string
  url?: string
  needVerify?: boolean
}
