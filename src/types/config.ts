// eslint-disable-next-line import/prefer-default-export
export enum CookiesMapEnum {
  IS_COOKIES_AGREEMENT_ACCEPTED = 'is_cookie_agreement_accepted',
  BEARER_AUTH_TOKEN = 'bearer_token',
  IS_INIT_TEST_PASSED = 'is_init_test_passed',
  IKS_USER_TOKEN = 'iks_user_token'
}

export enum HeadersMapEnum {
  BEARER_AUTH_TOKEN = 'x-bearer-auth-token'
}
